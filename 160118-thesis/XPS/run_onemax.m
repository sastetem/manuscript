
nbxps = 100;
correct = 0;
porcent = 0;

for i=1:nbxps
    [evals, FX,X] = oneplusone('onemaxnoisy',10,1,1000,1);
    plot(evals,10 - feval('onemax',X),'black');
    ylim([0,10]);
    y1=get(gca,'ylim');
    hold on
    %plot([10*log(10) 10*log(10)],y1);
    %hold on
    if evals(end) <= 10*log(10)
        correct = correct+1;
        porcent = correct/nbxps*100;
    end
    %title(num2str(porcent))
end
function [evals,FXrec,Xrec,FXsearch,Xsearch] = oneplusone(funct,dim,noisetreatment,maxevals,discrete)

xopt = 0.5*ones(dim,1);
%initial pop
if discrete
    x = round(rand(dim,1));
else
    x = rand(dim,1);
end
FX = feval(funct,x-xopt);
FXrec = feval('sphere',x-xopt);
FXsearch = feval('sphere',x-xopt);
fx = FX;
Xrec = x;
Xsearch = x;
X = x;
evals = 1;
sigma = 0.1/sqrt(dim);

for i=2:maxevals
    if discrete
        onefifth = round(dim/5);
        flip = (1:1:dim)';
        auxflip = nchoosek(flip,onefifth)';
        sizeauxflip = size(auxflip);
        flipchosen = max(1,round(rand*sizeauxflip(2)));
        xmut = x;
        xmut([auxflip(:,flipchosen)]) = 1 - x([auxflip(:,flipchosen)]);
        fxmut = feval(funct,xmut);
    else
        xmut = x + sigma*randn(dim,1);
        fxmut = feval(funct,xmut);
        Xsearch = [Xsearch xmut];
        FXsearch = [FXsearch feval('sphere',xmut-xopt)];
    end
    if discrete
        if fxmut>fx
            x = xmut;
            fx = fxmut;
        end
    else
        if fxmut<fx
            x = xmut;
            fx = fxmut;
        end
        Xrec = [Xrec x];
        FXrec = [FXrec feval('sphere',x-xopt)];
    end      
    FX = [FX fx];
    evals = [evals i];
    X = [X x];
    %if all(x == ones(dim,1))==1
    %    break
    %end
end
end


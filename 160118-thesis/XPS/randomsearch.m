function [evals,FXrec,Xrec,FXsearch,Xsearch] = randomsearch(funct,dim,maxevals)

xopt = 0.5*ones(dim,1);
x = rand(dim,1);
fx = feval(funct,x-xopt);

Xrec = x;
Xsearch = x;
FXrec = feval('sphere',x-xopt);
FXsearch = feval('sphere',x-xopt);
evals = 1;

for i = 1:maxevals
    xsearch = rand(dim,1);
    fxsearch = feval(funct,x-xopt);
    Xsearch = [Xsearch xsearch];
    FXsearch = [FXsearch feval('sphere',xsearch-xopt)]; %to output, not used
        if fxsearch < fx
            x = xsearch;
            fx = fxsearch;
        end
    Xrec = [Xrec x];
    FXrec = [FXrec feval('sphere',x-xopt)]; %to output, not used
    evals = [evals i];
end


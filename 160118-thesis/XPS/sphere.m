function fx = sphere(x)
fx = sum(x.^2,1);
end
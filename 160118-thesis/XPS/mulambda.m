function [evals,FX,X,FXsearch,Xsearch,SIGMA] = mulambda(K,eta,mu,lambda,p,dim,noisetreatment,maxevals)
%p is the exponent of the objective function.
%initial parent
x = 100*ones(dim,1);

function fx = fpnoisy(x,p,sd)
[dim,N] = size(x);
fx = (sum(x.^2,1))^(p/2) + 1/sqrt(sd)*randn(1,N);
end

function fx = fp(x,p)
[dim,N] = size(x);
fx = (sum(x.^2,1))^(p/2);
end

sigma = ones(dim,1);
SIGMA = sigma;
SIGMAsearch = sigma;
FX = fp(x,p);
%FXrec = FX;
FXsearch = FX;
fx = FX;
%Xrec = x;
Xsearch = x;
X = x;
evals = 1;
tau = 1/sqrt(dim);
tau2 = 1/dim^0.25;

for t=2:maxevals
    %xmut = zeros(dim,lambda);
    %MUTATION AND EVALUATION
    for j=1:lambda
        %sigmamut = sigma; %*exp(1/sqrt(dim)*randn);
        chi = tau*randn;
        chi2 = tau2'*randn(dim,1);
        zeta = randn(dim,1);
        sigmamut = (sigma.*exp(chi2))*exp(chi);
        xmut = x + sigmamut.*zeta;
        if strcmp(noisetreatment,'exp')
            sd = ceil(K*eta^t);
            fxmut = fpnoisy(xmut,p,sd);
        elseif strcmp(noisetreatment,'adapt')
            sd = ceil(K*(1/sigma)^eta);
            fxmut = fpnoisy(xmut,p,sd);
        elseif strcmp(noisetreatment,'poly')
            sd = ceil(K*t^eta);
            fxmut = fpnoisy(xmut,p,sd);
        end        
        SIGMAsearch = [SIGMAsearch sigmamut];
        Xsearch = [Xsearch xmut];
        FXsearch = [FXsearch fxmut];
    end
    %SELECTION   
    Xparents = Xsearch(:,end-lambda+1:end);
    FXparents = FXsearch(end-lambda+1:end);
    SIGMAparents = SIGMAsearch(:,end-lambda+1:end);
    [sortedF,sortedIndex] = sort(FXparents);
    muparents = Xparents(:,sortedIndex(1:mu));
    muparentsF = FXparents(:,sortedIndex(1:mu));
    musigma = SIGMAparents(:,sortedIndex(1:mu));
    %UPDATE
    x = mean(muparents,2);
    X = [X x];
    sigma = mean(musigma,2);
    SIGMA = [SIGMA sigma];
    evals = [evals evals(end)+lambda*sd];
    FX = [FX fp(x,p)];
end
end


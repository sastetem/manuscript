function fx = onemaxnoisy(x)
[dim,N] = size(x);
fx = sum(x)+round(randn(dim,N));%+round(random('logn',0,1));
end
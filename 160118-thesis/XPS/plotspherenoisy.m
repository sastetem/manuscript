x=[-5:0.05:5];
fx = feval('spherenoisy',x);
plot(x,fx);
hold on
[fxmax,xmaxind] = min(fx);
xmax = x(xmaxind);
plot(xmax,fxmax,'*');
fx = feval('sphere',x);
plot(x,fx);
plot(0,0,'+');
leg=legend(strcat('sphere with noise'),strcat('(',num2str(xmax),...
    ',',num2str(fxmax),')'),strcat('sphere'),...
    strcat('(',num2str(0),...
    ',',num2str(0),')'));
set(leg,'interpreter','tex');
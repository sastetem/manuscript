function f = polylogarithmic(n)
a = [1 1 1];
f = 0;
for i=1:length(a);
    f = f + a(i)*(log(n))^i;
end
end
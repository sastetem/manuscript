function fx = fp(x,p)
[dim,N] = size(x);
fx = (sum(x.^2,1))^(p/2);
end

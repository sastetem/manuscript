function fx = spherenoisy(x)
%x  a matrix with column vectors
[dim,N] = size(x);
fx = sum(x.^2,1) + randn(1,N); %randn;%+ randn(dim,N);
end
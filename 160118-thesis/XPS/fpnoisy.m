
function fx = fpnoisy(x,p,sd)
[dim,N] = size(x);
fx = (sum(x.^2,1))^(p/2) + 1/sqrt(sd)*randn(1,N);
end

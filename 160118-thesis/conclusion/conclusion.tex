\chapter{Conclusion}
\label{chapter:conclusion}

We explore in this thesis the study of Noisy Optimization Algorithms. We propose generalizations of popular algorithms and show mathematically several properties such as their convergence order and rate. We support the theoretical results with experiments, using the sphere function with noise for the continuous case. We exhibit  a critical analysis on the use accuracy measure in order to compare the performance of algorithms. 
Finally, we expand the convergence consequences to the case of discrete noisy optimization and determine the convergence rates in that case. \\
The remainder of this chapter states the conclusions related to each chapter in the Part~\ref{part:contributions}: Contributions.

\section{Chapter~\ref{chapter:inoa}: Convergence Rates for General Noisy Optimization Algorithm}
We propose an algorithm called ``Iterative Noisy Optimization Algorithm'' (see Algorithm~\ref{alg:inoa}) that generalizes noisy optimization algorithms by dividing the processes into \emph{search}, \emph{recommendation} and the dependency of some parameters that determine step-size and reevaluation. The algorithm uses the process $\search$ to create new search (or sample) points using as input the previous recommendation, the step-size and the evaluation number. The process to generate recommendations is called $\opt$ and it uses the previous recommendation and the search points and their function evaluations (possibly reevaluations of the same search point). The algorithm is complemented by a property called ``Low Square Error'' (LSE), see Definition~\ref{def:lse}, that states basically that recommendations are close to the optimum if search points are. 
\\
We prove that two important linesearch algorithms in noisy optimization can be implemented by INOA and they satisfy the LSE: the noisy gradient method and the noisy Newton method. Both the gradient and Newton method presented here use approximations of the gradient and the Hessian, therefore they are zero order methods. %\todoist{NOTATION}{put the names noisy gradient and noisy newton to what we use}
\\
We prove general convergence rates for the noisy Newton method. The analysis yields that with the appropriate parameters, the noisy Newton method can reach convergence rates described in the literature with several algorithms. We also prove a conjecture raised on \cite{jebalia_multiplicative_2008}. We refer the reader to table~\ref{table:summaryINOA} to see a summary of our results and how they compare with the literature.

\section{Chapter~\ref{chapter:convES}: Log-log Convergence for Evolution Strategies}
We start by assuming that the result on \cite{auger_convergence_2005} yields the convergence of ES on the noise-free case for the sphere function. In other words, we take the precise result on \cite{auger_convergence_2005}, which proves the convergence of the sequence $1/n \log (x_n)$ to a constant, and we assume that this constant is negative. The assumption is backed up by experimental work in the literature.% \todoist{CITE}{search some xps}. 
\\
We propose then a scheme of ``reevaluation'' included in the ES. The reevaluation allows for a good estimation of the real function value, in the case of noisy optimization. The reevaluation depends on a two parameters $K$ and $\eta$ and the iteration number. We analyze three type of reevaluation: exponential, adaptive and polynomial (see table~\ref{table:revalschemes} for details). We obtain theoretical order of convergence for exponential and adaptive schemes. We evaluate experimentally the convergence of the polynomial scheme
\\
We obtain in theorem~\ref{thm:exp} that an ES that converges log-linear on the noise-free case for the sphere, converges log-log on the noisy $p$-sphere (see definition on~\ref{def:noisyFunction}) provided that it possesses an exponential reevaluation scheme. Note that we assume to be in presence of scale invariance. We extend the result in theorem~\ref{thm:adapt} by getting rid of the scale invariance and now considering and adaptive reevaluation scheme. We also obtain a log-log convergence in this case. For the polynomial scheme we realise several experiments using combinations of the parameters. The experiments suggest log-log convergence. 
\\
Note that we cannot say anything about the convergence rate, only that it seems to be $s(SR) \geq -1/2$.

\section{Chapter~\ref{chapter:lowerboundES}: Lower Bound on the Convergence Rate of Evolution Strategies}
This is a natural continuation of Chapter~\ref{chapter:convES} where we prove a lower bound for the slope of the Simple Regret. 
\\
We start by generalizing an optimization algorithm, something similar to the generalization in Chapter~\ref{chapter:inoa}. But in this case we consider the \emph{search}, \emph{recommendation} and a random element that allows the inclusion of randomized algorithms. 
% PARA DESPUES Note that we do not specify anything about the parameters of reevaluation or stepsizes, as in~\ref{chapter:inoa}. 
We define Evolution Strategies as the algorithms that satisfy special conditions for the search process (see Section~\ref{subsec:simpleES}) and we denote them by \emph{Simple Evolution Strategies} (Simple ES). The Simple ES considered at this point do not include algorithms that generate search points far away from the optimum. We obtain that the slope of the Simple Regret is lower bounded by $-1/2$ for the sphere function. The important consequence is that for any family that contains the sphere function will be subject to this lower bound when being optimized by a Simple ES.
\\
We provide some experimental verification on figures~\ref{fig:uhcmaesareslow} and~\ref{fig:oneplusoneesareslow} using two ES: UHCMAES and 1+1 ES. We contrast the result of the ES with a noisy linesearch method that uses a one point gradient estimation technique, Shamir Algorithm, figure~\ref{fig:shamiresareslow}. The Shamir Algorithm has a theoretical convergence rate of $-1$ for the Simple Regret, which can be observed on the experiments presented in this chapter. On the contrary, the ES exhibit the lower bound of $-1/2$ theoretically exposed before.

\section{Chapter~\ref{chapter:asr}: Performance measure of Noisy Optimization Algorithms}
In Chapter~\ref{chapter:asr} we focus on the analysis of the performance measure in noisy optimization algorithms. That is, the object of the study is the performance measure itself and not the performance of an algorithm. The work presented on the previous chapters usually uses the convergence of Simple Regret. We analyze if there is convergence of the sequence and the rate of convergence. With this information we are able to say which algorithm has a better performance over a problem.
\\
But the empirical analysis of algorithms does not allow to obtain the sequence of Simple Regret in noisy optimization. Mainly because the noise prevents us to have access to real function value. We analyze two alternative definitions to Simple Regret and we name then Approximate Simple Regret and Robust Simple Regret. Both of them non-increasing, which can be useful for the use on a real testbed. The important thing should be that the alternative definitions ``approximate'' well the Simple Regret.
\\
We prove our results using three elements: the literature, conjectures based on the literature and mathematical proofs. The results conclude that the use of these approximations of Simple Regret lead to misleading results. The Robust Simple Regret acts as a lower bound for Simple Regret, but it is not a tight bound. For Evolution Strategies we obtain that the Approximate Simple Regret overestimates their performance, while in the case of noisy linesearch algorithms estimating the gradient we obtain that the Approximate Simple Regret underestimates their performance. We obtain results from an experiment (see figure~\ref{fig:asr}) that exhibit the behaviour clearly: the performance  algorithms using Simple Regret is not the same as the performance using Approximate Simple Regret. 
\\
The results on the poor approximation of Simple Regret by these other measures that attempt to approximate it on real testbeds constitutes an important problem. It is essential to use the adequate performance measures when comparing algorithms. The theoretical analysis and the empirical analysis has to be coherent in order to continue to develop the study of noisy optimization. Unfortunately we are not able to provide such an approximation of Simple Regret, but this work at least shows that the choice of a performance measure on empirical analysis is not obvious and it needs to be justified.

\section{Chapter~\ref{chapter:discrete}: Convergence Rates using Reevaluation in Discrete Noisy Optimization}
The reevaluation schemes used in Chapter~\ref{chapter:convES} are also applicable to discrete noisy optimization problems in Chapter~\ref{chapter:discrete}. We assume that a discrete optimization problem (e.g. OneMax, LeadingOnes) is perturbed by some continuous noise (Gaussian or any noise with bounded variance). 
\\
As in previous results, we define a general process of optimization $\opt$ that takes as input the search points and its  function values, and it returns a recommendation point for the current iteration, for the noise-free case. The noisy counterpart of $\opt$ will be named $K$-$\opt$. The parameter $K = (K_i)_{i=1}^n$ refers to a sequence  of length $n$ that determine the number of reevaluations of the $n$ search points that serve as input. 
\\
We analyze the runtime of $K$-$\opt$ in two cases: the runtime of $\opt$ over the noise-free problem is known or unknown. On the first case, if we know the runtime for $\opt$ on the noise-free discrete problem, say $r$, then the runtime of $K$-$\opt$ over the noisy counterpart of the discrete problem is $O(r \log r)$. This results holds whether the noise is Gaussian or heavy tail. For the second case, we assume we do not have prior information on the runtime of $\opt$ for the noise-free problem. We also assume that $\opt$ satisfies a \emph{criterion} (see definition~\ref{eq:criteria}) with high probability. The criterion extends the runtime concept. We obtain as a result that $K$-$\opt$ also solves (i.e. reaches the criterion) the noisy problem and we give an estimate of the number of function evaluations on iteration $i$ for the case of Gaussian noise. If the noise is any heavy tail random variable, then we have an analogous result, but the number of function evaluations is greater than the Gaussian case.  


		
%\section{Discussion}
	

%\section{Perspectives}		
			

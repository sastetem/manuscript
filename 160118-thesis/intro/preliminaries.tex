\chapter{Preliminaries}
\label{chapter:preliminaries}
%\addcontentsline{toc}{chapter}{Introduction}

%\begin{textblock}{0.7}[0.0,0.0](0.07,0.01)
%\paragraph{Statement:}
%
%\end{textblock}
%\vspace*{2.5cm}

\section{Basic Notation}

We denote the positive integers by $\N$, the reals by $\R$ and the positive reals by $\R_+$. 
We denote by $\omega$ a random variable with the following precise definition.
\begin{align*}
\omega: \Theta &\to \R \\
\theta 		&\mapsto \omega(\theta) ,
\end{align*}
with distribution $D_\omega$ such that for any $a\in\R$, $D_\omega (a) = \P ( \{ \theta \in \Theta: \omega(\theta) \leq a \})$. \change{Typically} we will omit the dependence in $\theta$. We denote by $\E_\omega$ the expectation with regard to the random variable $\omega$. When it is not necessary, we will omit in the notation the dependency on $\omega$ and only use $\E$.
 $[a,b]$ will denote the interval between $a$ and $b$. \change{ It can also denote the set $\{ a,a+1,\ldots,b\}$ when we treat with discrete functions (Chapter~\ref{chapter:discrete}).}
 $\mathscr{C}^{k,p}_L(\dom)$ represents the class of functions $k$ times continuously differentiable on $\dom$, with $p$-th derivate Lipschitz with constant $L$.
 
 \section{Optimization Problem and Performance of Numerical Methods}
 \label{sec:nesterov}
 Let $x$ be a $d$-dimensional real vector: $x = (x^{(1)}, \ldots, x^{(d)})$, a set $\dom \in \R^d$ and $F$ some real valued function of $x$. Then, we search for
 \begin{align}
 \min_{x\in \dom} F(x).
 \end{align}
% There are several classifications optimization problems: based on the functions (objective and constraints), based on the properties of the feasible set $\dom$ or by the types of solutions. 
We will call our minimization problem $\problem$. Assume that we want to find the best method to solve $\problem$. This means that we want to find a method that solves some \emph{class} or \emph{family} of problems, say $\family$, where $\problem$ is a member of the class. Since we want to measure the performance of the algorithm on a family, we need to take that into account that we have only the information of the family $\family$ when we evaluate the performance of an algorithm. We introduce the concept of \emph{oracle} $\oracle$. The oracle is a machine that answers the successive calls of the method. We will consider throughout this thesis an oracle of \emph{zero order}: the oracle has access to the function evaluation of any point $x\in \dom$. We could also consider oracles of \emph{first order} and \emph{second order}, which have access also to the gradient and Hessian of the objective function respectively. We will use \emph{black-box/zero order algorithm} to specify that the \emph{oracle} used by the algorithm is black-box/zero order.
\emph{Black-box} is a characteristic of the oracle. It means that the only information available for the method is the answers from the oracle.
\\
The performance of a method $\method$ on $\problem$ is the total amount of \emph{computational effort} required by $\method$ to \emph{solve} $\problem$. We need to define more precisely \emph{computational effort} and \emph{solve}. These definitions can make a big difference on the performance of a given method. We will consider \emph{computational effort} to be as the  \emph{complexity} of the problem $\problem$ for the method $\method$: the number of calls to the oracle which is required to solve $\problem$. The term \emph{solve} is related to some \emph{accuracy} that the method needs to accomplish for their approximation to be considered as a solution of the problem $\problem$. One of the most natural ways to measure the accuracy is using the difference between the real optimum $x^*$ and the approximated optimum output by the algorithm, $\hat x$ in the image space. That is $F(\hat x) - F(x^*) \leq \epsilon$ (we say $\hat x$ an $\epsilon$-solution).
\\
The complexity and convergence rate are related. The convergence rate measures how fast the sequence of approximated points converges to the optimum. This can be measured also on the image space. If we have access to the convergence rate of the method, we can  impose that $F(x_t) - F(x^*) \leq \epsilon$ and obtain the $t$ such that the solution at time $t$, $x_t$ is an $\epsilon$-solution of the problem.


 \section{Test Functions}
 \label{sec:functions}
 Throughout this thesis we study the noisy counterpart of a deterministic function. We denote by $F$ the deterministic function and $f$ its noisy counterpart. Their definitions are as follows: 
 \begin{align}
 F &: \dom \to \R, \qquad x \mapsto F(x) \\
 \label{def:additive}
 f&:(\dom,\Theta) \to \R, \qquad (x,\theta) \mapsto f(x,\omega(\theta)) = F(x) + \omega(\theta).
 \end{align}
 
 If $\dom \in \R^d$ the problem is a \emph{continuous problem} or if $\dom \in \Z^d$ the problem is a  \emph{discrete problem}. We denote simply by $f(x,\omega(\theta)) = f(x,\omega)$. In order to lighten the notation, we will only use $f(x,\omega)$ when $\omega$ is relevant. Otherwise, we will use $f(x)$, always keeping in mind the dependence of $\omega$, therefore the stochastic nature of $f$.
 
 \subsection{Sphere Function}
 A large part of the analysis presented in this thesis is based on the sphere function. We define here its basic form.
 \begin{definition}
 Let $d \in \N$. Then the sphere function is defined as follows:
 \begin{align*}
F: \R^d & \to \R \\
 x &\mapsto || x-x^*||^2.
 \end{align*}
 \end{definition}

\section{Modes of Convergence}
For a deterministic sequence, there is only one definition of convergence: 
\begin{definition}[Convergence of a deterministic sequence]
Let $(x_t)_{t\geq 1} \in \R^d$. Then $(x_t)_{t\geq 1}$ converges to $x^*$ if for all $\epsilon >0, \exists T>0$ such that for all $t\geq T$, 
\begin{equation}
|| x_t - x^*|| \leq \epsilon \nonumber.
\end{equation}
The convergence uses the notations: $\lim_{t \to \infty} x_t = x^*$ or $x_t \to x^*$ among others.
\end{definition}
Whilst for random variables there are severals definitions not equivalent. In some cases one type of convergence is sufficient to have another type of convergence. \nnew{For instance, convergence in probability (Definition~\ref{def:convprob}) is a consequence of the almost sure convergence (Definition~\ref{def:convas}). Nonetheless, the reciprocal  is not true.}
\begin{definition}[Almost sure convergence]
\label{def:convas}
The sequence $\rvX_t$ of random variables converges to a random variable $\rvX$ almost surely (a.s.) if 
\begin{equation}
\P ( \lim_{t \to \infty} \rvX_t = \rvX) = 1 \nonumber.
\end{equation}
\end{definition}
\begin{remark} Almost sure convergence is equivalent to convergence with probability one. \end{remark}
\begin{definition}[Convergence in probability]
\label{def:convprob}
The sequence $\rvX_t$ converges in probability to a random variable $\rvX$ if for all $\epsilon > 0$
\begin{equation}
\lim_{t \to \infty} \P ( || \rvX_t - \rvX|| \geq \epsilon) = 0 \nonumber.
\end{equation}
\end{definition}

\section{Convergence Order of Deterministic Sequences}
Let a deterministic sequence $(x_t)_{t \geq 1}$ converge to $x \in \R^d$, and assume for all $t, x_t \neq x$. Let
\begin{align}
\label{eq:deterministicconv}
\lim_{t \to \infty} \frac{|| x_{t+1} - x||}{|| x_t - x||^q} &= \mu, && \text{with $q \geq 1, \mu \in (0,1)$}.
\end{align}
We define the \nnew{\emph{order of convergence of the sequence} $(x_t)_{t \geq 1}$ } depending on $q$ and $\mu$ in \nnew{Definitions ~\ref{def:superlinear}, ~\ref{def:linear} and ~\ref{def:sublinear}. They correspond to the \emph{super-linear, linear and sub-linear} convergence.}
\begin{definition}[Super-linear] \nnew{The order of convergence is \emph{super-linear} if any of the following conditions is satisfied:}
\label{def:superlinear}
	\begin{itemize}
		\item $\mu = 0$,
		\item $q>1$,
		\item $\mu >0$. In this case we say \emph{convergence with order } $q>1$. If $q=2$, we say the convergence is \emph{quadratic}.
	\end{itemize}
\end{definition}

\begin{definition}[Linear]\nnew{The order of convergence is \emph{linear} if }
\label{def:linear}
$q=1$ and $\mu \in (0,1)$. In this case we obtain:
		\begin{align}
			\label{eq:linearconv}
			\lim_{t \to \infty} \frac{|| x_{t+1} - x||}{|| x_t - x||} &= \mu / .
		\end{align}

\end{definition}

\begin{definition}[Sub-linear]\nnew{The order of convergence is \emph{sub-linear} if }
\label{def:sublinear}
$q=1$ and $\mu=1$. We can extend the definition to \emph{convergence with degree} $p>0$ if
	\begin{align}
	\label{eq:sublinearconv}
		\lim_{t \to \infty} & \frac{|| x_{t+1} - x||}{|| x_t - x||} = 1 - c_t || x_t - x||^{1/p} / , && \text{with } c_t \to c>0 / .
	\end{align}
	Note that if $p\to \infty$ and $c <1$ we get linear convergence and note that Equation~\ref{eq:sublinearconv} implies
	\begin{align}
		 || x_t - x|| &\sim \left( \frac{p}{c}\right)^p \frac1{t^p} \nonumber
	\end{align}
\end{definition}

\section{Convergence of Random Variables}
Consider now $(x_t)_{t \geq 1}$ a sequence of random variables. The definitions in Equations~\ref{eq:deterministicconv} and~\ref{eq:linearconv} are no longer appropriate in general. We define a weaker version of linear convergence, \nnew{analogous to the definition linear convergence} in~\ref{eq:linearconv} (see  \cite{auger_theory_2011-1} for details).
\begin{definition}[Log-linear convergence]
The sequence $(x_t)_{t \geq 1}$ converges \emph{log-linearly} almost surely if $\exists c>0$ such that:
	\begin{align}
			\label{eq:loglinearconv}
			\lim_{t \to \infty}\frac1t \log \frac{|| x_{t} - x||}{|| x_0 - x||} &= c \quad \text{  a.s.}
	\end{align}
The sequence $(x_t)_{t \geq 1}$ converges log-linearly in expectation or in mean if $\exists c>0$ such that:
	\begin{align}
			\label{eq:loglinearconv}
			\lim_{t \to \infty}\frac1t \E \left( \log \frac{|| x_{t} - x||}{|| x_0 - x||} \right) &= c \ .
	\end{align}
\end{definition} 
Following the same notation as in deterministic sequences, if the sequence $||x_t - x ||$ converges to 0 following $1/t^p$ we say that the sequence \emph{converges sub-linearly with degree} $p$.

\subsection{Simple and Cumulative Regret}
In this thesis we fix our attention to one parameter to determine the rate of convergence of an algorithm. First of all, allow us to make a difference between \emph{search} and \emph{recommendation} points. The \emph{search points} are all the points explored by the algorithm where the objective function is evaluated. The \emph{recommendation points} are the points the algorithm output as approximations to the optimum. The search and recommendation points can be the same, but note that the algorithm could produce recommendation points without having to evaluate them.
\\
 In this section we will define Simple and Cumulative Regret. The Simple Regret refers to the most natural way to measure the accuracy of an optimization method: measure the difference between the \emph{recommendation} and the optimum. In other words, we only look at the points the algorithm outputs as approximations to the optimum. On the contrary, the Cumulative Regret, as the name suggests, considers the cumulative \emph{cost} of evaluating points. Therefore, in this case we consider \emph{all} the points that are being evaluated throughout the optimization process. Therefore all the search points are considered. We denote by $x_t$ the sequence of search points and $\hat x_t$ the sequence of recommendation points.
Following the definition of sub-linear convergence, we obtain $	|| \hat x_t - x || \sim \frac{1}{t^p}. $
\nnew{Or, in other words}
	\begin{align}
	 \log (|| \hat x_t - x||) \sim - p \log (t)\ . \nonumber
		%	& || \hat x_t - x || \sim \frac{1}{t^p} \nonumber \\
	%\implies 	& \log (|| \hat x_t - x||) \sim - p \log (t). \nonumber
	\end{align}
Therefore, when $-p<0$, then $|| x_t -x||$ is converging to 0. And if we look at a plot using as x-axis the $\log (t)$ and the y-axis as $\log(|| \hat x_t - x||)$ we obtain that the line on the plot should have \emph{slope} $-p$. When $-p$ is close to 0, then the approximations approach the optimum slowly (if approaching at all). When $-p$ negative with big absolute value, that means that the approximations approach to the optimum fast.
\\
We denote the \emph{Simple Regret at iteration t} by $SR_t$ and define it as follows, considering the recommendation points and their distance to the optimum $x^*$ in the image space instead of the search space.
	\begin{align}
	\label{def:SR}
		SR_t = \E_\omega (f(\hat x_t,\omega) - f(x^*,\omega)) = F(\hat x_t) - F(x^*) \ .
	\end{align}
Note that we need to consider the expectation since the function $f$ is stochastic. Since the convergence order in the cases studied in this thesis are usually \emph{sub-linear}, to study the convergence rate we will observe the behaviour of Simple Regret $SR_t$ with regards to $t$. That is, we will observe the \emph{slope} on the log-log graph. We define then the \emph{slope of the Simple Regret} (denoted $s(SR)$) as follows
	\begin{align}
		\label{def:slopeSR}
		s(SR) = \lim_{t \to \infty} \frac{\log (SR_t)}{ \log (t)}	\ .
	\end{align}
 We will also consider another accuracy measure: the \emph{Cumulative Regret} (denoted CR). The CR is used widely in the bandit community in order to measure the \emph{expected gain (or loss)} when there are several options to take. It considers \emph{all} the points that are evaluated, measuring therefore the cost of exploration. 
 	\begin{align}
	\label{def:CR}
		CR_t = \E_\omega \left[ \sum_{\tau =0}^t (f( x_\tau,\omega) - f(x^*,\omega)) \right] = \sum_{\tau = 0}^t F( x_\tau) - F(x^*) \ .
	\end{align}
 To study the convergence rate we define the slope of the Cumulative Regret analogous to the Simple Regret
 	\begin{align}
		\label{def:slopeCR}
		s(CR) = \lim_{t \to \infty} \frac{\log (CR_t)}{ \log (t)}	\ . 
	\end{align}
 Note that it is equivalent to say $s(SR) = -1$ and $SR_t = O (t^{-1})$, with $O(\cdot)$ the usual Landau notation.
 
 \section{Useful tools}
 \subsection{Properties of Random Variables}
 Let $\rvX: \Omega \to \R$ be a random variable.
 \begin{lemma}[Linearity of Expectations]   Let $\rvX, \rvY$ be random variables, $\alpha,\beta \in \R$. Then,
 \begin{equation}
 \E [\alpha \rvX + \beta \rvY] =  \alpha \E [ \rvX]+ \beta \E [ \rvY ] \ . \nonumber
 \end{equation}
 \end{lemma}
 
  \begin{lemma}[Independence]   Let $\rvX, \rvY$ be independent random variables. Then
 \begin{equation}
 \E [\rvX \rvY] =  \E [ \rvX]  \E [ \rvY ] \ . \nonumber
 \end{equation}
 \end{lemma}
  
 \begin{lemma}[Properties of the Variance] \label{lemma:variance}Let $\rvX, \rvY$ be random variables, $\alpha,\beta \in \R$. Then
 \begin{enumerate}
 \item $\var [\alpha] = 0 \ . $
 \item $\var[\alpha + \beta \rvX] = \beta^2 \var [\rvX]\ . $
 \item $\var[\rvX + \rvY] = \var [ \rvX ] + \var[\rvY ]\ . $
 \item $\var [\rvX] = \E [\rvX^2] - (E [\rvX])^2\ . $
 \end{enumerate}
 \end{lemma}
 
 \begin{lemma}[Transformation of Random Variables] 
 \label{lemma:trans} Let $\rvX$ be an absolute continuous random variable with density $f_\rvX$. Let $\beta \neq 0$, $\alpha \in \R$. Then the random variable $\rvY =  \alpha + \beta \rvX$ is absolute continuous with density $f_\rvY$ given by:
 \begin{equation}
 f_\rvY (y) = \frac{1}{|\beta|} f_\rvX \left( \frac{y-\alpha}{\beta}\right), \qquad y \in \R. \nonumber
 \end{equation}
 \end{lemma}
 %\todoist{THEO}{Extension to dimension d ? }.
 
 \subsection{Inequalities}
 
 \begin{lemma}[Markov's inequality]
 \label{lemma:markov}
 Let $\rvX$ be a non-negative random variable.   Then for any real number $\epsilon>1$
 \begin{equation}
 \P [ \rvX \geq \epsilon \E[\rvX]] \leq \frac1{\epsilon} \ . \nonumber
 \end{equation}
 \end{lemma}
 
 \begin{lemma}[Chebyshev's inequality]
 \label{lemma:chebyshev}
 Let $\rvX$ be a random variable with finite expectation $\mu$. Then for any real number $k>0$
 \begin{equation}
 \P [ | \rvX - \mu| \geq \epsilon ] \leq \frac{\E [| \rvX - \mu|]^2}{\epsilon^2} \ .  \nonumber
 \end{equation}
 \end{lemma}
 
 \begin{lemma}[Bertrand series]
 \label{lemma:bertrand}
 The Bertrand series is defined as $\sum_{i=2}^n \frac1{i \log i^\beta}$ and
 \begin{align}
 \lim_{n\to \infty} &\sum_{i=2}^n \frac1{i \log i^\beta} < \infty \ , && \text{for any } \beta>1. \nonumber
 \end{align}
 \end{lemma}
 \subsection{Bounds}
 
 \begin{lemma}
 Let $a,b  \in \R_+$ such that $b \geq 1$. Then
 \begin{equation}
 (1-a) \leq b(1-a^{\frac1b}) \ .  \nonumber
 \end{equation}
 \end{lemma}
 

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}

	This thesis describes some of the work done in the context of the Symbrion project\footnote{IP, FP7, 2008-2013}. This project targets the realization of complex tasks which require the cooperation of multiple robots within robotic swarms (at least 100 robots operating together). Among issues studied by the project are the self-assembly of robots to form complex structures and the self-organization of large number of robots toward the realization of a common task. Subjects of interests are thus modular self-adaptive robots with both strong coordination properties, and swarm-level cooperation.
	
	The challenge faced by this project is that robots are used in open environments which remain unknown until their deployment. Since operational conditions can't be predicted beforehand, on-line learning algorithms must be used to design behaviors. In the use of large groups of robots, multiple considerations have to be taken into account: reduced communication abilities, small memory storage, small computational power. Therefore on-line learning algorithms have to be distributed among robots.
	
	Multiple approaches have already been proposed to deal with on-line decentralized learning of robotic behaviors, such as probabilistic robotics, reinforcement learning or evolutionary robotics (all of which will be later described). However, the problem addressed here is even more complex as groups of robots are considered, rather than of a single robot. Moreover, due to the open-endedness and unpredictability of the environment, we can safely assume that the human engineer may lack the background knowledge necessary to sketch directions such that learning is possible. 
	
	As a matter of fact, ensuring the integrity of the swarm (e.g. simply surviving through energy recharge) is not only mandatory prior to address any further user defined task, but also an already very challenging problem.
%	However, such approaches require sufficiently structured environments and well defined tasks. Thanks to this, the background knowledge of the human engineer is sufficient to formulate the problem such that learning is possible.	
%	New problems arise when the world is open. That is to say, when events unknown to the human engineer can happen unpredictably. In this context, the world is largely unstructured, and even few informations are available to design robots able to ensure their own autonomy. Therefore, the first problem is to ensure the availability of robots as a service to the user available at all time.  This service can latter be used to perform a task, which can be optimized or be a succession of orders.
% 	Robots integrity is the first mandatory element to the ensure that robots are available as a service to the user. 
 	Hence, the problem of ensuring integrity should be placed as the first element of the following roadmap, which we assume as a set of necessary steps toward achieving tasks with a group of robots in open environments.
 	\begin{itemize}
 		\item Step 1: Ensuring the integrity of robots.
 		\item Step 2: Maintaining robots available as a service to the user.
 		\item Step 3: Achieve a user-defined task (optimized or not).
 	\end{itemize}
 	
 	In this thesis we address the step 1 of this roadmap, and states the following:

\begin{textblock}{0.7}[0.0,0.0](0.07,0.01)
\paragraph{Statement:}
Collective robotics in open environment requires to perform self-adaptation prior to address user-specified task. %Therefore the following question is at the core of the research presented here:
% ajouter que c'est un moyen pour ensuring integrity ?
\end{textblock}
\vspace*{2.5cm}
 	
 	The subject of interest of this thesis, is to design a decentralized algorithmic solution that can be used to guaranty swarm integrity in open environment, using collective robot system with local communication.
%\begin{textblock}{0.7}[0.0,0.0](0.07,0.01)
%\paragraph{Question}
%How to design a self-daptive collective robotic system guarantying its own integrity ?
%\end{textblock}
%\vspace*{2.5cm}
This is a difficult problem which hasn't been addressed as such in the literature, even if trails exist (see Chapter~\ref{chap:edea}). The main difficulty in the resolution of this problem is the need to take into account the environment. Indeed, robots may have to display a large variety of behaviors at the global scale such as cooperation, specialization, altruism, and division of labor, depending on the environment at hand.

Solving such an environment driven adaptation task can be seen as requiring to satisfy two possibly antagonist motivations. The first motivation is to guaranty the integrity of a maximum of robots within the group in the environment at hand (extrinsic motivation). The second motivation is to allow the necessary interactions between robots and environment to ensure the well functioning of the algorithm (intrinsic motivation). There is therefore a trade-off between conservative approaches (remain stationary, and thus satisfy the intrinsic motivation, event at the risk of preventing the realization of the task), and exploratory approaches (test every possible interactions, and thus satisfy the extrinsic motivation, even at the risk of breaking a robot or getting lost).

	
In this thesis we introduce and define the problem of Environment-driven Distributed Evolutionary Adaptation. We  propose an algorithm to solve this problem, which has been validated both in simulation and on real robots. This algorithm has been used to study self-adaptation problems in specific environments:
\begin{itemize}
	\item Environments where behavioral consensus is required (see Chapter~\ref{sec:edea-mEDEA})
	\item Environments where robustness in front of environmental changes is required (see Chapter~\ref{chap:envtChange})
	\item Environments where altruistic behaviors is required (see Chapter~\ref{chap:altruism})
\end{itemize}
	
	\section*{Organization}
	
	The first chapter of this thesis presents a partial review of the robotic field. This review briefly presents the different aspects of the robotic field. A particular focus is given to the design of controllers for robots deployed in challenging environments.
	
	The second chapter aims at presenting the Evolutionary Robotics field. The focus is given to algorithms used in robots during their deployment. We also present a contribution made to this field as an illustrative example.
	
	The third chapter presents the issues faced by the decentralized autonomous optimization of behaviors for robots deployed in unknown environments. Different methods known to partially address these are reviewed. After highlighting the strength and weakness of each method, the EDEA domain is introduced. 
	
	In the fourth chapter the minimal EDEA algorithm, termed \textsc{mEDEA} is presented. The ability of \textsc{mEDEA} to reach consensus is presented in simulation and real world experiments.
	
	The fifth chapter is focused on the capacity of \textsc{mEDEA} to address changing environments. Notably, the main evolutionary dynamics of the algorithm are shown.
	
	The sixth chapter shows the evolution of altruistic behaviors by the \textsc{mEDEA} algorithm in front of challenging environments. Moreover, the mechanism at play during the evolution of altruistic behaviors are analyzed.
	
	The seventh chapter conclude this Ph.D. thesis. A discussion on the work done is presented, and perspectives for future works are highlighted. 
	
	%	Addressing these issues in a large range of open environments is a challenging task. Indeed, in this type of environment the exact conditions in which robots will operate can't be predicted beforehand. Consequently, the optimum behavior can't be pre-designed, and has to be optimized autonomously. This can be done during a conception phase, which would use large computational power, and produce a solution optimized once for all. This type of approach is therefore restricted to robots deployed in stable environments which can be simulated. Another way to perform autonomous design, is by relying on robots during their deployment. It results that lower computation resources are available, and solutions are continuously adapted in the environment at hand, i.e. this approach can be used for robots deployed in changing environments.	
%	While large groups of robots are efficient to solve challenging tasks, their design implies the use of low cost components with multiple limitations: reduced communication abilities, small memory storage, small computational power. As a consequence a centralized optimization process of robots' behaviors isn't possible (no robot has the computational power to do so), and global communications abilities aren't considered. Therefore, the autonomous adaptation algorithms considered is distributed among robots.

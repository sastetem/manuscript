\chapter{Runtime Analysis of Optimization Algorithms over Discrete Noisy Functions}
\label{chapter:discrete}

In this chapter we explore the runtime of algorithm for discrete noisy optimization. We inspire from the  work presented on the previous chapters and use a reevaluation scheme to adapt a regular algorithm for discrete optimization, in order for it to be able to handle the noisy version of the discrete problem.
\\
We obtain theoretical results for the runtime of the modified algorithms. We assume two cases. The first, we know the algorithm solves the noise-free discrete problem and we know its runtime. Second, we do not know the runtime of the algorithm over the noise-free discrete problem. 
\\
The work presented here deviates slightly from the results on the previous chapters, but only on the nature of the problems. We can see than applying the same type of principle, reevaluation of the points,  can also imply convergent algorithms on discrete noisy problems.
\\
We work here with \emph{runtime} instead of convergence rates for Simple Regret. In discrete optimization there is a preference to use the runtime of the algorithms since it gives easy and straightforward consequences on the performence. Ergo we use here the same concept.
\\
 The results presented are based on the journal publication \cite{akimoto_analysis_2015}. This is a joint work with Youhei Akimoto (Shinshu University) and Olivier Teytaud (TAO, INRIA Saclay, now Google).


\section{Algorithms applied to Discrete Optimization Problems}
\label{sec:applicationdiscrete}
Discrete optimization usually represent a completely different area, separated from continuous optimization. The discrete nature usually means there is a finite number of possible solutions. This fact can actually be an advantage in small cases when we can check all of the solutions. But discreteness also brings along dimensionality issues: the search space grows rapidly with the dimension. So much that the possible combinations for the solutions explode and it is no longer possible to check all of them. 
\\
In this thesis we do not explore the discrete optimization problems specifically. Nonetheless, we offer a result on a model of noisy discrete problems, in order to estimate the runtime of algorithms that can solve the noise-free discrete problem. The work is inspired by the use of Evolutionary Algorithms (see Chapter~\ref{chapter:convES}) over discrete problems. 
\\
The development of the studies of Evolutionary Algorithms is based on the study of classical discrete (or combinatorial) problems. These problems have been analyzed from other perspectives therefore we can find in the literature many results that describe the problems. Then the performance of the Evolutionary Algorithms has a base to be compared with: it is possible to place them as more or less efficient than other algorithms.
\\
For instance in \cite{auger_theory_2011-1}, Chapter 3 offers an analysis of well known discrete optimization problems. The work presented there shows how the analysis of these problems gives us insight on the functioning of Evolutionary Algorithms in general. Even more, the study of these problems shows the use of typical techniques on the analysis of  Evolutionary Algorithms. 
\\
Another example, is the work on \cite{rudolph_convergence_1994} who studies the runtime using Genetic Algorithms. Genetic Algorithms constitute a branch of Evolutionary Algorithms and they are characterized by the use of ``genes''. This means, the points on the search space represent a combination of genes and the aim of the algorithm is either maximize or minimize the fitness of a given gene.  The genes are an equivalent of human genes: they can express a finite number of values. Therefore, the Genetic Algorithms are most suited to work with discrete problems.
In the case of the work of \cite{rudolph_convergence_1994}, they study the maximization problem of the type
\begin{equation}
\max_{x \in B} F(x), 
\end{equation}
assuming $0 \leq F(x) \leq \infty$ for all $ x \in B = \{ 0,1\}^d$  and $F(X) \neq \text{constant}$. 
Using Markov Chain theory \cite{rudolph_convergence_1994} proves that canonical simple Genetic Algorithm using crossover and proportional selection do not converge, whilst the elitist version of it does converge. 
\\
One difference between the analysis of continuous and discrete problems is that the convergence, on the latter, depends mainly on the dimension of the search space. While as for the continuous problems we are concerned by the amount of function evaluations or iterations. 
\\
These thesis will present a modification on the algorithms ready to solve discrete problems in order for them to be also prepared to solve the noisy version of the discrete problems. We will use the same additive noise model as in the continuous problems. 






\section{Context}
In this work we provide an upper bound for the runtime of an algorithm adapted to handle noisy functions. The adaptation is done from an algorithm known to be able to solve the deterministic counterpart of the function. We deliver results depending on the previous knowledge on the runtime over the deterministic problem. If we know the runtime of the algorithm that solves the deterministic function, then we adapt it using that information and we derive the runtime of the adapted algorithm. If we do not know the runtime, we adapt the algorithm  in each iteration. In both cases the result is an algorithm able to solve the noisy counterpart of the deterministic problem, and we provide the runtime in the noisy case.

This work is focused on discrete optimization problems. The main motivation to develop is its immediate application to Evolutionary Algorithms (EAs). EAs have been successfully applied to discrete optimization problems \cite{laumanns_running_2002,droste_analysis_2002,muhlenbein_how_1992,storch_how_2006}. The theoretical analysis of EAs over discrete domain is usually carried out through the use of classical simple EAs over simple optimization problems. Let us define two simple discrete functions used widely for theoretical and experimental analysis: \nnew{$\onemax$ and $\leadingones$.}

 \begin{definition}
 \label{def:onemax}
 Let $d \in \N$. For $x^* \in \{ 0,1\}^d$ let
 \begin{align*}
 \om_{x^*}:  \{ 0,1\}^d &\to \N \\
 x &\mapsto \om_{x^*}(x) = | \{ i \in [0,n]\ | x^{(i)} = x^{*(i)} \}| \ .
 \end{align*}
 Let
 \begin{equation}
 \onemax_d := \{ \om_{x^*} | x^* \in \{ 0, 1\}^d  \} \ .
 \end{equation}
  \end{definition}
 
  \begin{definition}
  \label{def:leadingones}
 Let $d \in \N$. For $x^* \in \{ 0,1\}^d$ let
 \begin{align*}
 \lo_{x^*}:  \{ 0,1\}^d &\to \N \\
 x &\mapsto \lo_{x^*}(x) = \max \{ i \in [0,n] | \forall j \in [1,\new{i}], x^{(j)} = x^{*(j)} \} \ .
 \end{align*}
 Let
 \begin{equation}
 \leadingones_d := \{ \lo_{x^*} | x^* \in \{ 0, 1\}^d  \} \ .
 \end{equation}
 \end{definition}


For instance, the performance of the algorithms RLS and $(1+1)$-EA over the problems of \nnew{$\onemax$ and $\leadingones$}. The results on the literature roughly indicate that the runtime for $\onemax_d$ is $\theta (d \log d)$ and for $\leadingones_d$ is $\theta(d^2)$ \cite{auger_theory_2011}. %\todoist{CITE}{check cite}. 

EAs are not only used on deterministic setups. They are naturally robust in the presence of actuator noise\footnote{\nnew{The actuator noise refers to a perturbation in the search space. In this thesis we focus on perturbation in the image space, as the additive noise.}} \cite{jong_are_1992,deb_analysis_2004}. Nonetheless, other studies point out that EAs need to be modified to handle additive or multiplicative noise \cite{jebalia_log-linear_2011}. %\todoist{PLOT}{Put easy plot of 1+1 with and without reevaluation over onemax}.
The modifications include mainly the use of surrogate models \cite{ong_global_2003,caballero_algorithm_2008,booker_optimization_1998,leary_derivative_2004} or some reevaluation technique \cite{arnold_general_2006} % \todoist{CITE}{More cites for resampling?}. 
Surrogate models basically build a model that replaces the need to ask to the real function for evaluations, therefore saving up function evaluations. On the other hand, reevaluation aims to decrease the variance by reevaluating the same point several times and averaging its function evaluations to get a better estimate of the real function evaluation. The reevaluation technique has been studied both theoretically and experimentally. 
The work presented by us on \cite{akimoto_analysis_2015} focuses on the study of the adaptation of reevaluation techniques from continuous to discrete setup, for functions perturbed by additive noise with constant variance. 

We define an optimization algorithm by the sequence of search points it generates. Let $\opt$ be the algorithm that solves a class $G=\{g \in G | g:\dom \to \Z\}$ of discrete deterministic functions. The sequence generated by $\opt$ is defined by
\begin{equation}
\label{eq:opt}
x_{n+1} = \opt (x_1,\ldots,x_n, y_1,\ldots,y_n) \ ,
\end{equation}
where for every iteration $i$, $x_i \in \dom$ is the search points and  $y_i = g(x_i)$ is the function evaluation of point $x_i$. 

We define now $K$-$\opt$, the algorithm that is adapted to handle the noisy counterpart of G. 
Since the functions evaluations in this case are perturbed by noise,  $K$-$\opt$ reevaluates the point $x_i$ several times and then averages the results and assigns this mean as the function evaluation of point $x_i$.
The parameter $K$ represents a sequence that defines the number of reevaluations of the search points. In other words, $K=(k_i)_{i \geq 1}$, where $k_i$ is the number of reevaluations of the search point $x_i$. The Algorithm  $K$-$\opt$ then can be \nnew{defined in a similar fashion as the Algorithm} $\opt$ in (\ref{eq:opt}).
\begin{equation} 
\label{eq:kopt}
x_{n+1} = K\text{-}\opt (x_1,\ldots,x_n, \hat y_1,\ldots, \hat y_n) \ .
\end{equation}
\nnew{$K$-$\opt$ depends} on search points and on a mean function evaluations of the each search point.  The way to obtain the function evaluations is the main difference between $\opt$ and $K$-$\opt$.

\section{Known runtime, first hitting time}
\subsection{Known runtime, Gaussian noise}

This section presents the theorem that states the runtime of $K$-$\opt$ when we know in advance the runtime of $\opt$ over the deterministic family of functions. The technique and the result explored in this section is similar to the one on \cite{gutjahr_runtime_2012}. Nonetheless, several differences separate both works. We consider mono objective functions (opposite to \cite{gutjahr_runtime_2012} that uses bi objective), with a wide class of algorithms and a large family of functions. Also, we consider two noise models: Gaussian and heavy-tail.
From the work of \cite{qian_effectiveness_2014} we know that using reevaluation directly on $(1+1)$-EA to optimize OneMax (see definition~\ref{def:onemax}) with Gaussian noise is not enough. In fact, the expected first hitting time increases as the number of reevaluation increases. The result presented here can be applied over $K$-$(1+1)$-EA, not over $(1+1)$-EA directly. 
\\
We define the \emph{runtime} as follows:
\begin{definition}[Runtime $r(\delta)$ of algorithm $\opt$ to solve $G$] The runtime $r(\delta) = r(\delta,G)$ is the number of fitness evaluations needed before $\opt$ evaluates the optimum of any function $g\in G$, with probability at least $1-\delta$.

\end{definition}
We define also $G+\sigma \gauss$, the noisy counterpart of the family $G$, solved by $\opt$. The following Theorem states the runtime of $k_\gauss$-$\opt$ to solve $G+\sigma \gauss$. 
\begin{theorem}
\label{thm:gaussian}
Assume $\opt$ solves $G$ with runtime $r(\delta)$. Let 
\begin{equation}
\label{eq:kgauss} 
k_\gauss = \max \left(1 , 32 \sigma^2 \left( \log(2) - \log \left(1-(1-\delta)^{1/r(\delta)} \right) \right)  \right) \ ,
\end{equation}
then, $k_\gauss$-$\opt$ solves $G+\sigma \gauss$ with probability at least $(1-\delta)^2$ and runtime $O \left( r(\delta) \max \left( 1, \sigma^2 \log \left( \frac{r(\delta)}{\delta}\right) \right) \right) \ .$
\end{theorem}

\begin{proof}
%%%%%PROOF 1st DRAFT
%Let noisy fitness be the hat y, real fitness y. k opt sees hat y and opt sees y.
%We compute the probability of hat y to be sufficiently close to y. In this context, sufficiently close means that the difference is less than 1/4, so that k opt assigns to x its real fitness value. We compute the complementary event, p=prob(hat y and y to be separated by more than 1/4). 
%The difference between hat y and y follows a Gaussian distribution with variance sigma2/k. So we have
%(1-p) r(delta) is the probability that the distance between hat y and y is upper bounded by 1/4 for all samples in a run of length r(delta). 
%Therefore,
%delta geq blabla
%Finally, we obtain the overall number of function evaluations r delta times k gauss
%%%%%END PROOF 1st DRAFT

Let $y$ be the fitness value of a point $x$ and $\hat y$ its noisy fitness value. That is to say $\hat y = \frac{1}{k_\gauss} \sum_{i=1}^{k_\gauss} f(x,\omega_i)$. We will prove that probability of $k_\gauss$-$\opt$ to make a ``mistake'' is very small.  We will prove then that $k_\gauss$-$\opt$ obtains exactly the same results as $\opt$, with probability $(1-\delta)$.  
Let us compute $p$ the probability of $k_\gauss$-$\opt$ to make a mistake on one particular point $x$. In order for the mistake to arise, $\hat y$ and $y$ apart by at least $1/4$. In that case $k_\gauss$-$\opt$ assigns to $x$ a fitness value different than the real one.
\begin{align*}
p 	&= \P \left( | \hat y - y | > \frac14 \right) 	\ ,	\\
 	&= \P \left( | \gauss | >  \frac14 \frac{\sqrt{k_\gauss} }{ \sigma}\right) 	&& \text{using }| \hat y - y | \sim \gauss(0,\sigma^2/k_\gauss) \ , \\
	&\leq 2 \erfc \left( \frac{1}{4} \frac{\sqrt{k_\gauss} }{\sqrt{ 2 \sigma^2} } \right)  		&& \text{erfc is the complementary error function} \ , \\
	&\leq 2 \exp \left( - \frac{1}{4^2} \frac{\sqrt{k_\gauss} }{2 \sigma^2 } \right)  		&& \text{using} \erfc (x) \leq \exp (-x^2) \ .\\
\end{align*}

Plugging the definition of $k_\gauss$ (Equation~\ref{eq:kgauss}) on the latter upper bound for $p$ and some simple algebraic operations, we conclude that $p \leq 1 - (1-\delta)^{\frac1r}$. 
Or equivalently,
\begin{equation}
\label{eq:pmistake}1 - (1-p)^r \leq \delta \ .
\end{equation}
We note that the left side of Equation~\ref{eq:pmistake} is exactly the probability of $k_\gauss$-$\opt$ to make a mistake at least once on the assigning of fitness value for a point $x$ based on its noisy fitness value $\hat y$. The difference between the algorithms is that for every evaluation that $\opt$ does, $k_\gauss$-$\opt$ does $k_\gauss $ evaluations. 
Therefore, since $\opt$ finds the optimum of the deterministic problem in runtime $r$ with probability $(1-\delta)$, then $k_\gauss$-$\opt$ finds the optimum of the noisy problem in runtime $r k_\gauss$ with probability $(1-\delta)^2$. To obtain the runtime of $k_\gauss$-$\opt$ we assume that  $r \geq 1$ and the general inequality $(1-a) \leq b (1-a^{1/b})$ for $0 \leq a \leq 1$ and $b \geq 1$ we obtain directly from the definition of $k_\gauss$. 
\begin{equation*}
k_\gauss = O \left( \sigma^2 \log \left( \frac{r}{\delta} \right) \right) \ .
\end{equation*}
Therefore, the runtime of  $k_\gauss$-$\opt$ is $\displaystyle{ r k_\gauss = O \left( r \max \left( 1, \sigma^2 \log \left( \frac{r}{\delta} \right) \right) \right) }$.
\end{proof}

\subsection{Known runtime, heavy tail noise}

We prove in this section that the case with heavy tail noise has a larger upper bound than the Gaussian noise. The proof is very similar, the main difference is that we cannot longer compute directly the distribution: we only know that the variance is finite. 

Define $G+\Psi$, the noisy counterpart of the family $G$, solved by $\opt$. Now $\Psi$ is any random variable with bounded variance $\sigma^2$.
\begin{theorem}
\label{thm:heavytail}
Assume $\opt$ solves $G$ with runtime $r(\delta)$. Let 
\begin{equation}
\label{eq:kheavy} k_\Psi = \max \left(1 , 16 \sigma^2 \frac{1}{1-(1-\delta)^{1/r(\delta)} }   \right) \ .
\end{equation}
Then, $k_\Psi$-$\opt$ solves $G+\Psi$ with probability at least $(1-\delta)^2$ and runtime $O \left( r(\delta) \max \left( 1, \sigma^2   \frac{r(\delta)}{\delta} \right) \right)$.
\end{theorem}

\begin{proof}
%%%%%PROOF 1st DRAFT
%We use chebysevh to prove that prob(hat y - y geq 1/4 at least once ) leq 1 - (1 - 16 sigma 2/k) r(delta). 
%If k geq 16 sigma2/1 - (1-delta)1-r(delta), then eq (just above) is smaller than delta. Therefore we can conclude that if OPT solves G with prob at least (1-delta) within r(delta) , then kheavy - opt solves G+sigma N with proba at least (1-delta)2  within r(delta)*kheavy leq r(delta).......
%%%%%END PROOF 1st DRAFT
We use the same notation as the proof of Theorem~\ref{thm:gaussian}. The proof is analogous, with the following differences. We do not have access to the exact distribution of $\hat y - y$, so we use Chebyshev's  inequality to upper bound the probability of mistake, %\todoist{VOC}{not successful instead of mistake? } 
$p$, as follows:
\begin{equation}
\label{eq:upperboundp}
p = \P ( | \hat y - y | \geq \frac14 ) \leq \frac{16 \sigma^2}{k_\Psi}
\end{equation}
We plug the definition of $k_\Psi$ on inequality~\ref{eq:upperboundp} to obtain the same bound as in~\ref{eq:pmistake}. 
Therefore, since $\opt$ finds the optimum of the deterministic problem in runtime $r$ with probability $(1-\delta)$, then $k_\Psi$-$\opt$ finds the optimum of the noisy problem in runtime $r k_\Psi$ with probability $(1-\delta)^2$. The runtime is $\displaystyle{ r k_\Psi = O \left( r \max \left( 1, \sigma^2   \frac{r}{\delta}  \right) \right) }$.
\end{proof}

\section{Unknown runtime, general performance measure}

The previous section shows how to choose how many reevaluations of each point we should take in order to maintain the convergence of the method with a similar probability. We have used implicitly in the definition of runtime the ``first hitting time'' criterion, since we count evaluations until we reach the optimum and evaluate it for the first time. This is certainly not the only possible criterion. For instance we might use epsilon distance and be satisfied to have an approximation at a distance at least $\epsilon$ from the real optimum.  Another possibility is the first time we reach an ``stable'' optimum: the number of fitness evaluations $t^*$ necessary such that for all $t \geq t^*$, the current approximation of the optimum is the best, with probability $1-\delta$. Anyway, the thing is that we may want to change what we consider to be a ``good enough'' approximation, depending on the circumstances. 

Therefore we define a general criterion that is either reached or not at iteration $T$. The formal definition is:
\begin{equation}
\label{eq:criteria}
\begin{aligned}
Q: \cup_{t \in \N} \dom^t \times \Z^t & \to \{0,1\}   \\
  \left( (x_t) , (y_t) \right) &\mapsto  Q\left( (x_t) , (y_t)  \right) \ ,
\end{aligned}
\end{equation}
where $Q\left( (x_t) , (y_t) \right) = 1$ if the criterion is reached. Note that the criterion $Q$ can be evaluated at any time $t$. 
\\
We will assume that $\opt$ satisfies a criterion $Q$ for any $T \in \N$ with probability at lest $(1-\delta)$ for any objective function $g \in G$. In other words,
\begin{equation}
%\label{eq:criterion1}
\forall g \in G, \forall T \in \N, \qquad \P \left[Q\left( (x_t)_{t \leq T} , (y_t)_{t \leq T}  \right) = 1 \right] \geq 1- \delta \ .\nonumber
\end{equation}
We will prove that we can define a sequence that allow the algorithm to reach the criterion. The sequences will be named $K_\gauss = (k_\gauss^i)_i $ for the case of Gaussian noise, and $K_\Psi = (k_\Psi^i)_i$ for the case of heavy tail. 
%These sequences allow the algorithm $K_\gauss$-$\opt$ to reach the criterion $Q$ with $O(T \log( T ))$ function evaluations, in the case of Gaussian noise. For the case of heavy tail noise, the criterion $Q$ is reached by the algorithm $K_\Psi$-$\opt$ after $O(T^2 \log( T )^ \beta)$ for some $\beta>1$. 
\subsection{Gaussian noise}
The following theorem proves $K_\gauss$-$\opt$ reaches the criterion $Q$ with probability at least $(1-\delta)$. 
\begin{theorem}
\label{thm:gaussianany}
Assume $\opt$ solves $G$ with probability $1-\delta$. Let $\new{\beta}>1$ and
\begin{equation}
\label{eq:kgaussi}
k_\gauss^i = 32 \sigma^2 \log \left( \frac{2(i+1) \log (i+1)^\beta}{\delta} \left( \sum_{j=2}^\infty \frac1{j \log(j)^\beta}\right)  \right) \ .
\end{equation}
Then $K_\gauss$-$\opt$ solves $G + \sigma \gauss$ with probability at least $(1-\delta)^2$. Additionally, the total number of fitness evaluations up to the $i$-th iteration is $O(i \log i)$.
\end{theorem}
\begin{proof}
First, note that the term $\sum_{j=2}^\infty \frac1{j \log(j)^\beta}$ is a Bertrand series and it is convergent as $n\to\infty$ (Lemma~\ref{lemma:bertrand}).
\\
We define $u_i = \P ( |\hat{y_i} - y_i | \geq 1/4)$ and $v_i = \P( \exists l \leq i:  |\hat{y_i} - y_i | \geq 1/4)$. Using the same arguments as \nnew{the ones used} in the proof of Theorem~\ref{thm:gaussian}.
\begin{align*}
u_i 	&\leq 2 \exp \left( \frac{-\sqrt{k_\gauss^i}}{32 \sigma^2} \right) \\
	&\leq \frac{\delta}{(i+1) \log (i+1)^\beta \sum_{j=2}^\infty \frac1{j \log(i)^\beta}} && \text{using } k_\gauss^i \text{ defined in~\ref{eq:kgaussi}.} \\
\end{align*}
Then, by definition of $v_i$
\begin{equation*}
v_i \leq \sum_{j=2}^i u_j \leq \sum_{j=2}^\infty u_j \leq \delta \ .
\end{equation*}
Therefore, $K_\gauss$-$\opt$ satisfies the criterion $Q$ with probability at least $(1-\delta)^2$. The total number of function evaluations until iteration $i$ is $\sum_{j=1}^i k_\gauss^j \in O \left( \sum_{j=1}^i \log i \right) = O (i \log i)$.
\end{proof}

\subsection{Heavy tail noise}
The following theorem constitutes the analogous of the Theorem~\ref{thm:gaussianany}, but considering a family of functions with heavy tail noise $G + \Psi$. 
\begin{theorem}
\label{thm:heavytailany}
Assume $\opt$ solves $G$ with probability $1-\delta$. Let $\nnew{\beta}>1$ and
\begin{equation}
%\label{eq:kheavyi}
k_\Psi^i = 16 \sigma^2   \frac{ (i+1) \log (i+1)^\beta}{\delta} \sum_{j=2}^\infty \frac1{j \log(j)^\beta} \ .\nonumber
\end{equation}
Then $K_\Psi$-$\opt$ solves $G + \Psi$ with probability at least $(1-\delta)^2$. Additionally, the total number of fitness evaluations up to the $i$-th iteration is $O(i^2 (\log i)^\beta)$. %\todoist{PROOF}{Check, beta is not important right?}
\end{theorem}
\begin{proof}
The proof is analogous to the proof of Theorem~\ref{thm:gaussianany}, making the same changes as in the proof of Theorem~\ref{thm:heavytail}.
\end{proof}

\section{Application}

%\subsection*{Tightness}
%\todoist{DECIDE}{Should I put the tightness discussion? Maybe not....}
%\\
%\todoist{DRAFT}{This section is a 1st draft}
%\\
%The tightness of the result in the Gaussian case can be qualified as approximately tight. If we define T, Tnoise, we obtain using theorem Gaussian and (1-delta)2 geq 1-2delta: Tnoise = O(TlogT).  To ensure the tightness we also have to proof that the noisy case is in fact harder than the noise free. We do this by contradiction. 
%
%The heavy tail case's results are not tight. This is proved by a counterexample: Take the needle in the haystack problem with the first hitting time criterion. Then, to be sure to find the optimum the only strategy is to test each of the points on the search space. Both in the noisy and in the noise-free case. 
We state here some straightforward consequences on classical algorithms and problems. We apply the theorems presented in this chapter to know their runtime in the noisy case.
\\
First, we show in Figure ~\ref{fig:2noise} the effect of Gaussian noise and a heavy tail noise on the function evaluations. The Gaussian noise has parameters $\mu = 0$ and $\sigma =1$. For the heavy tail noise we use the log-normal distribution with parameters $\mu = 0$ and $\sigma =1$. We show here the distribution of the value of OneMax with the corresponding noise. We consider 100 noisy function evaluations a specific point in $\dom = \{0,1\}^{10}$. 
\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[trim={3cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/onemaxnoisy_hist.pdf}
		\caption{Gaussian Noise}
		\label{fig:onemaxnoisy}  
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
	%\fbox{ 
		\includegraphics[trim={3cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/onemaxnoisyheavy_hist.pdf} 
		%}
		\caption{Heavy tail Noise}
		\label{fig:onemaxnoisyheavy}  
	\end{subfigure}
	\caption{\label{fig:2noise} Gaussian and heavy tail, dimension 10, histogram of 100 evaluations over x = (1111100000) for function OneMax. The real function value is OneMax$(x) = 5$, but we will get perturbed values due to the effect of the noise}
\end{figure}
In Figure~\ref{fig:onemax} we can see the result of the optimization process realized by $(1+1)$-ES with $1/5$ rule on the OneMax problem for dimension 10. We run the algorithm 100 times and we plot the difference between the function evaluation of the recommendation and the function evaluation of the optimum.  Therefore, we have good recommendations when the value of the y-axis is 0. The x-axis represents the iterations. And note that each of the lines represents a run of the $(1+1)$-ES.
\\
The left figure shows how the algorithm finds quickly the optimum (many times under 300 evaluations), even though sometimes it does not reach the optimum. We can compare it with the results in the figures of the center and right side. With the Gaussian noise we can observe how the algorithm struggles to get an optimum and even more, sometimes it finds it and loses it at the next iteration. The results get worse when we consider the heavy tail noise, where the algorithm is sometimes completely wrong. Evidently the noise has a big influence and the algorithm needs some way to cope with the noise.  

\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[trim={3cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/onemax_oneplusone.pdf}
		\caption{OneMax deterministic}
		\label{fig:onemaxnoisy}  
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
	%\fbox{ 
		\includegraphics[trim={3cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/onemaxnoisy_oneplusone.pdf} 
		%}
		\caption{OneMax Gaussian Noise}
		\label{fig:onemaxnoisyheavy}  
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
	%\fbox{ 
		\includegraphics[trim={3cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/onemaxnoisyheavy_oneplusone.pdf} 
		%}
		\caption{One Max Heavy tail Noise}
		\label{fig:onemaxnoisyheavy}  
	\end{subfigure}
	\caption{\label{fig:onemax} 100 runs of 1+1 EA with 1/5th rule over OneMax dimension 10}
\end{figure}
We present the results in Table~\ref{table:applytheorems}. The first column refers to the problem and the second column to an algorithm used to solve it. Third and fourth column are the runtime of the algorithm and the reference on the literature. The last two columns represent the  runtime on the noisy case: Gaussian and any noise with finite variance. Note that we neglect the precision as a function of $\delta$ and the exact computation of $K$, to focus on the runtime dependency on the dimension $d$.
\begin{table}[H]
\centering
\scriptsize
	\begin{tabular}{llrrrr}
	\toprule
	Problem		&$\opt$			& Runtime			& Reference					&  $k_\gauss$-$\opt$	& $k_\Psi$-$\opt$ \\
	\midrule
	OneMax		&$(1+1)$-EA		& $O(d \log d)$		& \cite{muhlenbein_how_1992}		&  $O(d (\log d)^2 )$		& $O(d^2 (\log d)^2 )$ \\	
	LeadingOnes	&Algorithm in  \cite{droste_upper_2006} 	& $O( d^2 )$		& \cite{droste_upper_2006}		&  $O( d^2 \log d)$		& $O( d^4 )$ \\
	MaxClique	&$(1+1)$-EA		& $O( d^5 )$		& \cite{scharnow_analysis_2004}	&  $O( d^5 \log d)$		& $O( d^{10} )$ \\	
	Sorting		&$(1+1)$-EA		& $O(d^2 \log d)$	& \cite{storch_how_2006}			&  $O(d^2 (\log d)^2 )$	& $O(d^4 (\log d)^2 )$ \\	
	\bottomrule
	\end{tabular}
	\
	\caption{\label{table:applytheorems} Runtime comparison of several discrete optimization problems in noise-free and noisy environment. For the runtime in noisy environment we use the results of Theorem~\ref{thm:gaussian} and~\ref{thm:heavytail} }
\end{table}

%\todoist{EXPLAIN}{Can I explain experiments with the plots ??}



\section{Conclusion}

We present a modification for algorithms that solve deterministic instances of discrete problems so that they can solve the noisy counterpart of the problem. The noise model considered is additive in two versions: Gaussian and heavy tail. We investigate the runtime of the modified algorithms in comparison to the runtime of the original algorithms.
The modified algorithm includes the reevaluation of the search points several times, in order to reduce the variance, and the assignation of the function value by averaging the reevaluations and using a rounding function to assign a discrete function value. 
If the original algorithm has a known runtime over the deterministic case, we use a number of reevaluation depending on the runtime for the original algorithm, constant in each iteration of the algorithm. For the Gaussian noise we obtain a runtime almost the same as the runtime on the noise free case (extra logarithmic factor) and for the heavy tail noise the runtime is quadratic on the original runtime.
\\
If the original algorithm does not have a known runtime on the deterministic case, we create a sequence of reevaluation number depending on the iteration and a parameter $\beta>1$. We can also extend the result by considering a general ``criterion'' that is reach by the algorithm in the deterministic case. In the case of the modified algorithm, the criterion is also reached in almost the same time as the original case, for Gaussian noise. And the runtime is quadratic for the heavy tail case. 

%\todoist{DECIDE}{The next paragraph only if tightness is included}
%The results obtained in this paper are almost tight for the case of Gaussian noise, this means that if OPT is an optimal algorithm for the deterministic case, then k-OPT is nearly tight. For the case of heavy tail, we present a counterexample of a problem with large complexity both in noise-free and noisy case. 




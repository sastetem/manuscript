\chapter{Lower bound rate of convergence ES}
\label{chapter:lowerboundES}

We continue the work of Chapter~\ref{chapter:convES} and determine now a lower bound for the convergence rate of Evolution Strategies. This lower bound is verified for the convergence of the Simple Regret, therefore we analyze the slope of Simple Regret: $s(SR)$. 
\\
We will present a general optimization algorithm, with processes of \emph{recommendation} and \emph{search}, but in a more general way than the algorithm presented in Chapter~\ref{chapter:inoa}. We use here a well characterised \emph{search} process to represent a simple Evolution Strategy. We also enumerate the necessary conditions  to attain the bound presented, and comment on the cases where the conditions are met.
\\
We obtain that $s(SR) > -1/2$ for simple Evolution Strategies that notably do not use search points far away from the optimum. The results are proved for a quadratic form, but they extend to any family that includes forms as the ones used here. We finish with an experimental verification of the theoretical result by comparing Evolution Strategies to a linesearch method for noisy optimization, found in \cite{shamir_complexity_2013}. We observe that while the ES do not perform better than the bound showed theoretically on this work, the linesearch method achieves the optimal convergence rate for quadratic functions: $s(SR) = -1$.
\\
 The results presented are based on the conference publication \cite{astete-morales_evolution_2015}. This is a joint work with Marie-Liesse Cauwet (TAO, INRIA Saclay) and Olivier Teytaud (TAO, INRIA Saclay, now Google).

\section{Context}
Evolutionary Algorithms are popular tools due to their wide applicability in optimization problems. In particular they show robustness in front of rugged fitness landscapes. %CITE
This feature translates into a strong advantage of Evolutionary Algorithm to optimize functions corrupted by noise.
\\
Evolution Strategies are a particular type of Evolutionary Algorithms  used in continuous optimization. Their \emph{mutation} operator creates an offspring by taking the parent of generation and adding some random perturbation to it. The random perturbation is usually Gaussian and controlled by a step-size. Therefore each offspring is produced from a random variable centered on the parent of the generation. This means that the mutation operator is more likely to create offsprings ``close'' to the parent of the generation.
\\ 
\subsection{Typical convergence behaviour}
The goal is to minimize the necessary  number of queries to the oracle to find a good recommendation. We measure the convergence of the Simple Regret with regards to the number of function evaluations. As we saw in Chapter~\ref{chapter:convES}, the ES converge in a log-log order, but we did not ensure the convergence rate. We find results of the log-log convergence order in the work of  \cite{arnold_local_2002,coulom_clop:_2012,decock_noisy_2013}.
\\
On the other hand, there are other algorithms that apparently have a better performance than the ES. We mention here linesearch method for noisy optimization, but we do not discard that there might be other type of algorithms also reaching optimal convergence rates (such as, algorithms that use models of the objective functions). In \cite{fabian_stochastic_1967} and \cite{shamir_complexity_2013} we find two linesearch methods of zero order that use different ways to approximate the gradient of the function. They both obtain log-log convergence order for the Simple Regret, with convergence rate $s(SR)=-1$. More precisely, \cite{shamir_complexity_2013} proves the convergence rate for strongly convex quadratic functions in a non-asymptotic way. And \cite{fabian_stochastic_1967} for a wider family of functions, asymptotically. 
\\
This work will in fact prove that there is a lower bound for the convergence rate of the ES. Therefore proving that they cannot reach the convergence rate of the optimal linesearch methods mentioned previously.

\section{Preliminaries}

Consider $f$ a noisy function with additive noise as presented in Section~\ref{sec:functions}. Noise is unbiased and with constant variance. The search points are $x_n$ at evaluation $n$ and the recommendations are $\hat x_n$. We denote the evaluations by $y_n = f(x_n,\omega)$. We denote also by $Z_n = \left( (x_0,y_0), \ldots, (x_{n-1},y_{n-1}) \right)$ the first $n$ pairs of search points and their respective noisy objective value. 
\\
We will study the convergence rate of the Simple Regret. In the proof, we will use a result from \cite{shamir_complexity_2013} on the Cumulative Regret for noisy optimization algorithms (see Theorem~\ref{thm:shamirCR}  for details).

\section{General Formalization of Algorithms}

An optimization algorithm basically samples some search points, evaluates them and proposes a recommendation based on the information it has available. We can formalize a general optimization algorithm as follows:

\begin{algorithm}[H]
\caption{\label{alg:genOptAlg} {General Optimization Algorithm}}
\begin{algorithmic}[1] %\scriptsize
	\Require $s$ random seed, $p$ parameters, $\internal$ initial internal state
   	\State $n = 0$
	\While{not finished}
		\State $r = rand(s)$
		\State $\hat x_n = \opt (Z_n,r,t,p,\internal)$ \Comment{Recommend}
		\State $x_n = \search (Z_n,r,t,p,\internal)$ \Comment{Search Point}
		\State $y_n = f (x_n,\omega)$ \Comment{Evaluation}
		\State $t = t+1$
	\EndWhile
\end{algorithmic}
\end{algorithm}

The procedure $\opt$ outputs a feasible point that stands as the approximation to the optimum. The procedure marked as $\search$ in Algorithm~\ref{alg:genOptAlg} outputs one or many search points that will be evaluated. Starting from $t=1$, both procedures use the information from previous iterations.

The framework presented in Algorithm~\ref{alg:genOptAlg} is in fact very general. Thanks to the parameter $r$ it includes possible randomized methods. Say we want to reproduce an algorithm that uses populations. Then a population of $\lambda$ search points can be output by $\search$ by splitting the offspring reproduction in $\lambda$ iterations, without varying the recommendation.  In summary, Algorithm~\ref{alg:genOptAlg} encodes black box algorithms, including ES and linesearch methods such as Shamir and Fabian. Note that we make no assumption on the distance between the search points and the recommendations. Algorithms like Shamir and Fabian use in their advantage the search points as exploration, so it is even desirable that they are far away from recommendations. On the other hand, ES use $\search$ procedure that concentrates search points around the current recommendation. 

%\subsection*{SCOPE OF THE ALGORITHM}
%. \todoist{THEO}{Not sure if putting this explanation on the internal state and how its not modified between calls to the oracle}.

\subsection{Simple Evolution Strategy}
\label{subsec:simpleES}
In the context of Algorithm~\ref{alg:genOptAlg}, we can specify the procedures to define an ES. In particular, we define the \emph{search distribution} of the algorithm. Normally the sampling for the search points in ES is made around the current recommendation, therefore we define $\search$ as:
\begin{equation}
\label{eq:mutationprocess}
\search(\zeta_t) = \opt(\zeta_t) + \sigma(\zeta_t) z(\zeta_t) \ .
\end{equation}
Where $\zeta_t = ( Z_t,r,t,p,\internal ) $. Usually  the stepsize $\sigma(\zeta_t)$ is updated at each iteration and sometimes for each coordinate. The random process  $ z (\zeta_t)$ is an independent $d$-dimensional random variable, with \nnew{expectation}:
\begin{equation}
\label{eq:mutationd}
\E( ||z (\zeta_t) ||^2 ) = d \ .
\end{equation}
We also assume that the ES satisfies the following condition:
\begin{align}
\label{eq:scaleinvES}
\nnew{
\exists C>0 \text{ such that } \forall t \geq 0  \quad \E (\sigma(\zeta_t)^2 ) \leq C \E ( || \hat x_t - x^*||^2) \ .
}
\end{align}
A \emph{Simple Evolution Strategy} (Simple ES) will be an optimization algorithm that satisfies all the conditions mentioned above.
\subsection{Evolution Strategies included in the formalization}
We proceed to detail \nnew{the ES that} can be considered as Simple ES and \nnew{the} ones are discarded.
\\
The assumption on Equation~\ref{eq:mutationd} is a light assumption. If it is not met, it can always be fixed by rescaling the stepsize $\sigma(\zeta_t)$ and afterwards satisfy the constraint.
\\
The assumption on Equation~\ref{eq:mutationprocess} refers to the mutation process of an ES. The process described above matches the classical mutation on ES for continuous optimization. It is verified on ES with single parent or a $\mu/\mu$ recombination, including weighted recombinations (for a complete overview on ES, see \cite{hansen_evolution_2015}).
\\
%.\todoist{THEO}{Ojo aca, se habla de scale invariance, pero la propriedad es con respecto a la recomendacion. Hay que aclarar que pasa entre los search points y las recomendaciones en las ES. INTRO.}.
The assumption on Equation~\ref{eq:scaleinvES} means that $\hat x_t$ and the stepsize $\sigma(\zeta_t)$ decrease at the same rate towards the optimum. It is verified in several situations according to literature:
%\todoist{CITE}{More references on scale invariance}.
\begin{itemize}
\item Scale invariant algorithm verify the property by definition. This assumption even if not realistic, it is used widely in theoretical analysis. %(CITE).
\item The results on  \cite{auger_convergence_2005} on the sphere prove that $\sigma_t/|| \hat x_t ||$ converges to a distribution. 
\item Verification a posteriori on experimental results when algorithms converge \cite{beyer_evolution_2002}, in self-adaptive algorithms \cite{hansen_completely_2001}, and in most of Evolutionary Algorithms \cite{beyer_theory_2001}.
\end{itemize}

Probably it is more useful to state which ES would not be covered among the Simple ES. Notably, algorithms that sample far away from the current estimate $\hat x_t$ in order to obtain or deduce more information on the function (as in surrogate models).


\section{Theorem: Lower bound for Simple ES}
We define the family $\mathcal{F}$ of all quadratic functions $F$ that satisfy:
\begin{align*}
F:\dom 	&\to \R \\
x 		&\mapsto \frac12 || x ||^2 - (x \cdot x^*) \ ,
\end{align*}
with $||x^*|| \leq 1/2$. We define its noisy counterpart \nnew{in the Definition~\ref{def:noisycounterpar}}.
\begin{definition}
\label{def:noisycounterpar} \nnew{The noisy counterpart of family $F$ denoted by $\mathcal{F}_\gauss$ is defined as follows:}
$$\mathcal{F}_\gauss = \{f | f(x,\omega) = F(x) + \omega, F \in \mathcal{F}, Var(\omega) = 1 \} \ .$$
\end{definition}

 Now we state the main result of this Chapter.
\begin{theorem}
\label{thm:esareslow}
Let a simple Evolution Strategy as defined in Section~\ref{subsec:simpleES} and assume that the simple ES solves $f \in \mathcal{F}_\gauss$. Then, for all $\alpha > \frac12$, 
\begin{equation}
s(SR) > -\alpha \ .\nonumber
\end{equation}
\end{theorem}
\begin{remark}
We assume here that we have a log-log convergence order and we only proof that if that is the case, then the convergence rate of the Simple Regret \emph{can not} be lower than $-1/2$.
\end{remark}
\begin{proof}
By contradiction, let us assume that $SR_t \leq \frac{D}{t^\alpha}$ for some $\alpha>1/2$ and $D>0$. To show the contradiction we will use Theorem~\ref{thm:shamirCR} that ensures that for a Simple ES there is at least one function $f \in \mathcal{F}$ for which $CR_t \geq 0.02 \min (1, d \sqrt{t})$. Let us compute $CR_t$ of the Simple ES:
\begin{align*}
	2CR_t&=2\sum_{i=1}^t \left(\E f(x_i,\omega_i)-f(x^{*})\right)  				&&\text{by Def. of $CR$ Eq.~\ref{def:CR}} \ ,\\
	    &=\sum_{i=1}^t \E \left( \|x_i\|^2-2(x^{*}\cdot x_i)+\|x^{*}\|^{2} \right) \ , \\
	    &=\sum_{i=1}^t \E \left( \|x_i-x^*\|^2 \right)  \ , \\
	    &=\sum_{i=1}^t \E \left( \| \hat x_i-x^*+\sigma_i\Phi_i \|^2 \right) 			&&\text{by Eq.~\ref{eq:mutationprocess} } \ , \\
	    &\leq \sum_{i=1}^t \left( \E\| \hat  x_i-x^*\|^2+\E \sigma_i^2 \E\Phi_i^2\right) 	&&\text{by independence} \ , \\
	    &\leq \sum_{i=1}^t\left( \E \| \hat x_i-x^*\|^2+d \E \sigma_i^2\right) 		&&\text{ by Eq.~\ref{eq:mutationd} } \ , \\
	    &\leq 2(1+dC)\sum_{i=1}^t \E ( SR_i )								&&\text{ by Eq.~\ref{eq:scaleinvES} } \ , \\
\implies & CR_t \leq D(1+dC)t^{1-\alpha} \ .
\end{align*}
Using Theorem ~\ref{thm:shamirCR} we have the contradiction.
\end{proof}

Let us note that Theorem~\ref{thm:esareslow} considers a particular type of family of quadratic functions, but the result applies for \emph{any} family of functions that includes sphere functions (i.e. functions in $\mathcal{F}$) with additive noise.
\\
With regards to the tightness of the result, it is not known whether ES reach $s(SR) = -1/2$. \cite{decock_noisy_2013} prove that an ES with Bernstein Races with modified reevaluations depending on the fitness of the population can reach $s(SR) = -\alpha$ with $\alpha$ arbitrarily close to $-1/2$. 

\section{Experimental Verification}
We present here experiments with three Algorithms: Shamir Algorithm, $(1+1)$-ES and \newline UHCMAES (\cite{hansen_method_2009}\footnote{For the experiments we use the algorithm in \url{ https://www.lri.fr/~hansen/ } with option ``uncertainty handle:on'' }). 
Shamir Algorithm reaches theoretically the convergence rate $s(SR)=-1$. The $(1+1)$-ES and UHCMAES are both contained in the set of algorithms concerned by the result in our Theorem of lower bound of the convergence rate for Simple Regret. We will see in this section that the experiments are consistent with the theoretical results.
\subsection{Fast convergence rate: Shamir Algorithm}
%.\todoist{INTRO}{Ojo shamir y spall estan relacionados, ver esareslow original}

The Shamir Algorithm, described in Algorithm~\ref{alg:shamir}, uses an estimation of the gradient to update its optimum at each iteration. The objective of this section is twofold: show how to write an algorithm in the form of the framework and show the experiments that exhibit the convergence rate of the algorithm. First we show that we can write an algorithm as the described framework in~\ref{alg:genOptAlg}\footnote{The reader \nnew{can} compare the original Algorithm~\ref{alg:shamir} with the framework presented here. Even though Algorithm~\ref{alg:shamirours} may seem more complicated, the advantage is that using the framework we have exact information on the different processes involved in the optimization}. Note that the $\search$ defines the search points, using some random process, represented by $r$. Shamir Algorithm is proved to reach asymptotically $s(SR) = -1$ in the quadratic case (in \cite{shamir_complexity_2013}). Notice also that this algorithm does not satisfy equation~\ref{eq:mutationprocess}  nor~\ref{eq:scaleinvES} therefore it cannot be considered as a Simple ES.

\begin{algorithm}[H]
%\scriptsize
	\caption{\label{alg:shamirours} \textsc{Shamir Algorithm}. Written in the general optimization framework. In this case $\internal$ is a vector of $t$ elements in the domain.}
    \begin{algorithmic}
    	\Procedure{$\rec$}{$x_0,\dots,x_{t-1}, y_0,\dots,y_{n-1},r,t,p,\internal$}
			\If{$|| \internal_t || \geq B$}
		\State{$ \internal_t=B\frac{\internal_t}{ || \internal_t ||} $}
		\EndIf
		\State{$\hat {x}_{t}=\frac{2}{t}\sum_{j=\lceil n/2\rceil,\dots, t}^{t}\internal_{j} $}
	\EndProcedure
    
    	\Procedure{$\sp$}{$x_0,\dots,x_{t-1},y_0,\dots,y_{t-1},r,t,p,\internal$}
		\If{$n = 0$}
		\State $\internal=(0)$
		\State Return $x_0=0$
		\EndIf		
			\State Compute $x_t = x_{t-1}+\frac{\epsilon}{\sqrt{d}}r$
			\State Compute $\hat g = \frac{\sqrt{d}y_{t-1}}{\epsilon}r$
			\State Compute  $\internal=(\internal,x_{t-1}-\frac{1}{\lambda t}\hat{g})$
	\EndProcedure
%		\Procedure{$f$}{$x_n,\w_n$} \State Compute $y_n=f(x_{n})$ \EndProcedure

\hrulefill

   %\Ensure 
   \State \textbf{Input:} $p = (\lambda, \epsilon, B) \in \R_+ \times (0,1] \times \R_+$, $s=\text{ random seed}$
	%\For{$n=0, \dots, 2(N-1)$}
        \State $t\leftarrow 0$ 
        \Loop
	\State Generate $r \in \{-1,1\}^{d}$, uniformly and randomly 
	\State $\hat x_t=\rec (x_0,\dots,x_{t-1},y_0,\dots,y_{t-1},r,t,p, \internal)$
	\State $x_t = \sp (x_0,\dots,x_{t-1},y_0,\dots,y_{t-1},r,t,p, \internal )$
	\State $y_t = f(x_t,\omega)$
	\State $t\leftarrow t+1$
	\EndLoop
    \end{algorithmic}
\end{algorithm}

Results in figure~\ref{fig:shamiresareslow} correspond to the mean of 21 runs of Shamir Algorithm for each dimension tested. Note that we plot directly the slope (or the convergence rate) of the Simple Regret. The function is the noisy sphere with optimum $x^* = 0.5$ and Gaussian noise. We observe that $s(SR)$ is always lower than $-1/2$, converging to $-1$. Always better than the lower bound we have proved in Theorem~\ref{thm:esareslow}.

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.45\textwidth]{esareslowFOGA/images_esareslow/shamir_esareslow_nbeval}
\end{center}
\caption{ \label{fig:shamiresareslow}Shamir Algorithm on the noisy sphere function. Average over 21 runs of the algorithm. The maximum standard deviation for all averages presented here is $10^{-3}$
}
\end{figure}

\subsection{Slow convergence rate: UHCMA and $(1+1)$- ES}
We test experimentally two ES. First, the UHCMAES (from \cite{hansen_method_2009}) that corresponds to CMA with an uncertainty handling tool to solve noisy objective functions. And second, the $(1+1)$- ES with reevaluation (see Chapter~\ref{chapter:convES} for more details on the reevaluation schemes). Both algorithms fall in the category of Simple ES. 
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{esareslowFOGA/images_esareslow/uhcma_esareslow_nbeval} 
\end{center}
\caption{\label{fig:uhcmaesareslow} UHCMAES Algorithm on the noisy sphere function. Average over 21 runs of the algorithm. The maximum std for all averages presented here is $1$
}
\end{figure}	
\\
Results in figure (\ref{fig:uhcmaesareslow}) correspond to the mean of 21 runs of UHCMA Algorithm for each dimension tested. The function is the noisy sphere $f(x,w) = F(x) + 0.3\omega$ where $\omega \sim \gauss (0,1)$ with optimum $x^* = 0.5$. %\todoist{XPS}{Repeat experiments with uhcma well adjusted and Gaussian noise?????}.

\begin{figure}[h]
\begin{center}
	\begin{subfigure}[b]{0.01\textwidth}
  \begin{adjustbox}{addcode={\begin{minipage}{0.1\width}}
  {\caption*{
    \hspace{2cm} \fbox{ Polynomial}
      }
      \end{minipage}}
      ,rotate=90,center}
  \end{adjustbox}
	\end{subfigure}
~	
	\begin{subfigure}[b]{0.3\textwidth}
	 %\fbox{
	\centering \includegraphics[scale=0.4]{esareslowFOGA/images_esareslow/oneplusone_esareslow_lognbeval_reval0_0} %}
	\caption{$\mathrm{reval}_n = n^0$}
	\end{subfigure}
~
	\begin{subfigure}[b]{0.3\textwidth}
	 %\fbox{
	\centering \includegraphics[scale=0.4]{esareslowFOGA/images_esareslow/oneplusone_esareslow_lognbeval_reval2_0} %}
	\caption{$\mathrm{reval}_n = n^2$}
	\end{subfigure}
~	
		\begin{subfigure}[b]{0.3\textwidth}
	 %\fbox{
	\centering \includegraphics[scale=0.4]{esareslowFOGA/images_esareslow/oneplusone_esareslow_lognbeval_reval8_0} %}
	\caption{$\mathrm{reval}_n = n^8$}
	\end{subfigure}
\\

	\begin{subfigure}[b]{0.05\textwidth}
  \begin{adjustbox}{addcode={\begin{minipage}{0.1\width}}
  {\caption*{
    \hspace{2cm} \fbox{Exponential}
      }
      \end{minipage}}
      ,rotate=90,center}
  \end{adjustbox}
	\end{subfigure}
		\begin{subfigure}[b]{0.3\textwidth}
	 %\fbox{
	\centering \includegraphics[scale=0.4]{esareslowFOGA/images_esareslow/oneplusone_esareslow_lognbeval_reval0_1} %}
	\caption{$\mathrm{reval}_n = 1^n$}
	\end{subfigure}
~
	\begin{subfigure}[b]{0.3\textwidth}
	 %\fbox{
	\centering \includegraphics[scale=0.4]{esareslowFOGA/images_esareslow/oneplusone_esareslow_lognbeval_reval2_1} %}
	\caption{$\mathrm{reval}_n = 1.03^n$}
	\end{subfigure}
~	
		\begin{subfigure}[b]{0.3\textwidth}
	 %\fbox{
	\centering \includegraphics[scale=0.4]{esareslowFOGA/images_esareslow/oneplusone_esareslow_lognbeval_reval8_1} %}
	\caption{$\mathrm{reval}_n = 1.07^n$}
	\end{subfigure}

\end{center}
\caption{\label{fig:oneplusoneesareslow} $(1+1)$-ES Algorithm on the noisy sphere function. Average over 400 runs of the algorithm. The maximum std for all averages presented here is $0.025$. First row of plots presents Polynomial reevaluation and the second row Exponential reevaluation.  
}
\end{figure}
Results in figure~\ref{fig:oneplusoneesareslow} correspond to the mean of 400 runs of $(1+1)$-ES Algorithm for each dimension tested. The function is the noisy sphere with optimum $x^* = 0.5$ and Gaussian noise. We observe that $s(SR)$ is never under $-1/2$, as predicted in Theorem~\ref{thm:esareslow}. %\todoist{XPS}{The experiments were done  with simple 1+1 or the improved??? CHECK!! }.

\section{Conclusion}

We have shown with Theorem~\ref{thm:esareslow} that Evolution Strategies, at least under their most common form, cannot reach the same rate as other noisy optimization algorithms, for instance the linesearch algorithms in \cite{shamir_complexity_2013,fabian_stochastic_1967}. The significant difference between these algorithms seems to be the $\search$ process. For the ES, the $\search$ process favors the points \emph{close} to the current recommendation, while  the linesearch algorithms do not satisfy this property. 
\\
The linesearch algorithms, Shamir and Fabian, both exhibit $s(SR) = -1$, non-asymptotically and asymptotically respectively. While we prove here that the ES can only reach $s(SR) = -1/2$ at most. This answers the conjecture raised in \cite{shamir_complexity_2013}. But, we should also point out that there is no proof that linesearch algorithms are the only ones able to reach optimal convergence rates.
\\
We notice also that Theorem~\ref{thm:esareslow} covers many pattern search methods. The only requierement is the form of the $\search$ process (Section~\ref{subsec:simpleES}), which we denote by \emph{simple Evolution Strategy}.  Nonetheless, the results do not cover Evolution Strategies with large mutations. This could be a good way to speed up the convergence of an Evolution Strategies.
\\
The experiments are done for the sphere function with additive noise. We contrast the result of ``fast`` linesearch methods and ``slow'' ES. We use for the linesearch the Shamir Algorithm (see Alg.~\ref{alg:shamir} or \cite{shamir_complexity_2013} for more information). For the ES we use the UHCMAES and $(1+1)$- ES. We confirm the theoretical results.

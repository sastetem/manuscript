\chapter{Log-log convergence of ES}
\label{chapter:convES}

In Chapter~\ref{chapter:inoa} we studied algorithms of the Linesearch family. In this chapter we step aside from those type of algorithms and  study Evolutionary Algorithms. We prove that a particular kind of Evolutionary Algorithms, the Evolution Strategies converge for noisy optimization problems. We use reevaluation to mitigate the noise and a convergence result for Evolution Strategies on noise-free optimization \cite{auger_convergence_2005}. 
\\
We present theoretical analyses for two types of reevaluation schemes: exponential and adaptive. We also consider scale invariance for the analysis of the exponential scheme. We ignore the scale variance and replace it by another property in the analysis of the adaptive scheme. 
We obtain that the order of convergence for Simple Regret is log-log in both cases. We also present experiments for a polynomial scheme of reevaluation, confirming the results obtained theoretically.
\\
 The results presented are based on the conference publication \cite{astete-morales_log-log_2014}. This is a joint work with Jialin Liu (TAO, INRIA Saclay, now University of Essex) and Olivier Teytaud (TAO, INRIA Saclay, now Google).

\section{Preliminaries}
In this section we present the three basic elements that constitute the results of this work. 
First, we start by analysing the convergence result from \cite{auger_convergence_2005}. We develop the results into the precise form we will use to prove the convergence of the Evolution Strategy for noisy problems. 
Second, we present the three different reevaluation schemes we will use in the analysis: exponential, adaptive and polynomial. And
third, we present the objective (or fitness) function we will use for the convergence analysis in this chapter.

\subsection{ Noise Free Case}
\label{subsec:noisefree}

From the analysis in Theorem 4, \cite{auger_convergence_2005} over the $\onelambdaSAES$ we know that considering $F(x) = ||x||^2$ the following results are satisfied almost surely:
\begin{align}
\label{eq:convxn} \frac1n \log ( ||x_n|| ) & \xrightarrow{ n \to \infty } R,  \\
\label{eq:convsigman}\frac1n \log ( \sigma_n ) & \xrightarrow{ n \to \infty } R.
\end{align}
If we assume $R$ is negative, then the Equation~\ref{eq:convxn} means that the sequence of recommendations of $\onelambdaSAES$ converges to the optimum in a log-linear scale. \\
We can write the Equation~\ref{eq:convxn} equivalently, for all $\epsilon>0$, and $n$ sufficiently large
\begin{align}
					%		& \left| \frac1n \log ( ||x_n|| ) - R \right|  < \epsilon \nonumber\\
\label{eq:star}  - \epsilon  < 	& \underbrace{ \frac{1}{n} \log ( ||x_n|| ) - R  < \epsilon }_{(\star)} .
\end{align}
Then,
\begin{align*}
(\star) 	& \implies \frac1n \log ( ||x_n|| ) \leq U && \forall U \text{ such that } R < U \\
		& \implies \underbrace{\log ( ||x_n|| ) - Un}_{:= A_n} \leq 0 \\
		& \therefore  \sup_{n \geq 1} A_n \leq 0. %\text{ a.s.}		
\end{align*}
\\
Let $Y = \exp (\sup_{n \geq 1} A_n) $ and $Q$ the $1-\delta/4$ quantile of $Y$. Therefore, with probability $1-\delta/4$:
\begin{align*}
		& \exp (\sup_{n \geq 1} A_n) \leq Q \\
 \implies 	&  \exp (  A_n) \leq Q \quad \forall n 	&& \exp(\cdot) \text{ is an increasing function} \\
 \implies 	&	|| x_n || \leq Q \exp (U n)		&& \text{using the definition of } A_n.
\end{align*}
We obtain an analogous lower bound for $|| x_n ||$ using the left hand inequality in~\ref{eq:star} as follows. We obtain that $B_n:= \log ( ||x_n|| ) - Ln\leq 0$, $\forall L$ such that $R>L$. Then we define $Z = \exp (\inf_{n \geq 1} B_n) $ and $q$ the $\delta/4$ quantile of $Z$. Therefore, with probability $1 - \delta/4$:
\begin{align*}
		& \exp (\inf_{n \geq 1} B_n) \geq q \\
\implies 	&  \exp ( B_n) \geq q \quad \forall n \\
\implies 	&	|| x_n || \geq q \exp (L n).
\end{align*}
We can use the same arguments to obtain upper and lower bounds for $\sigma_n$, starting from inequalities on (\ref{eq:convsigman}).
In summary we obtain that for any $U$, $L$ such that $L<R<U$, %\todoist{OJO}{Ojo con el orden de los cuantificadores. Existen las contantes tal que el evento ocurre con prob asd o existen las constantes con prob asd ??}
there exists $Q$, $q$, $Q_\sigma$, $q_\sigma >0$ such that with probability at least $1-\delta$, \new{for all $n$ sufficiently large}: 
\begin{align}
\label{eq:boundsxn} q \exp (L n) & \leq || x_n || \leq Q \exp (U n), \\
\label{eq:boundssigman} q_\sigma \exp (L n) & \leq \sigma_n \leq Q_\sigma \exp (U n).
\end{align}
Note that the bounds on~\ref{eq:boundsxn} and~\ref{eq:boundssigman} are interesting only when $R$ is strictly negative, so that we can choose $L$ and $U$ strictly negative as well.


\subsection{Algorithm $\mulambdaES$ with reevaluations}
We analyze Algorithm~\ref{alg:mulambdaesR} with different types of reevaluation schemes $r_t$: exponential, adaptive and polynomial. We abuse the notation and use always $K$ and $\eta$ for every type of reevaluation scheme, but they do not have to be the same for all three. We also consider the ceiling function of $r_t$ so we can ensure $r_t \in \N$ for all $t$.

\begin{table}[H]
\centering
%\scriptsize
	\begin{tabular}{lll}
	\toprule
	Type 		& Notation		&$r_t$  \\
	\midrule
	exponential	& $r_{K\eta}^{\text{exp}}$		&$K \eta^t $ 			 \\
	adaptive		& $r_{K\eta}^{\text{adap}}$ 	&$ K \sigma_t ^{-\eta} $ 	 \\
	polynomial	& $r_{K\eta}^{\text{poly}}$		&$K t^\eta $ 			 \\
	\bottomrule
	\end{tabular}
\caption{\label{table:revalschemes} Reevaluation schemes $r_t$}
\end{table}
The $\mulambdaES$ with reevaluation scheme $r_{K\eta}^{\text{exp}}$ will be called $\mulambdaESrexp$. Idem definition for adaptive and polynomial reevaluation schemes. Note that $\mulambdaESrexp$ preserves the same properties as $\mulambdaES$ with regards to the  $x_t$ and $\sigma_t$. The only difference is the amount of reevaluations made at each iteration.

\subsection{Fitness function}

We will consider the \emph{$p$ noisy function} defined for some $p>0$. % \todoist{THEO}{p greater than one?}. 
\begin{equation}
\begin{aligned}
f: \R^d \times \Omega & \to   \R  \\
  (x,\omega) &\mapsto  || x ||^p + \omega, && \text{with } \omega \sim \gauss,
\end{aligned}
\label{def:noisyFunction}
\end{equation}
\nnew{where $\gauss$ is either a Gaussian or a bounded variance distribution}. Note that for $p=2$ we obtain the noisy sphere function, for which we have the results on the noise-free case detailed in Section~\ref{subsec:noisefree}.

\section{Non adaptive exponential reevaluation and scale invariance}
\label{sec:expreval}

We will prove that if $\mulambdaES$ with scale invariance converges in the noise-free case, then it also converges in the noisy case, considering $\mulambdaES$ with exponential reevaluation when the parameters are large enough. We obtain the same log-linear convergence in terms of number of iterations, but once we take into account the extra number of evaluations done by $\mulambdaESrexp$, we observe log-log convergence.

%%%%  	THEOREM EXP   	%%%%%%%
\begin{theorem}
\label{thm:exp}
Assume $\mulambdaES$ solving the sphere function satisfies:
%\todoist{THEO}{Do we need this assumption? We dont have SA here so maybe this assumption not necessary? Comment later?}. 
\begin{enumerate}
\item For some  $L,U <0$, for any $\delta>0$ $\exists q_x,Q_x$ such that with probability $1-\delta/2$ for all $t$ sufficiently large,
\begin{equation}
\label{eq:boundsxn-1} q_x \exp (L t)  \leq || x_t || \leq Q_x \exp (U t). \\
\end{equation}
\item Scale invariance: For all $t$,
\begin{equation}
\label{eq:scaleinv} \sigma_t = C || x_t ||.
\end{equation}
\end{enumerate}
Then for any $\delta>0$, there is $K_0,\eta_0 > 0$ such that for all $K \geq K_0$, $\eta \geq \eta_0$, the points $x_t$ output by $\mulambdaESrexp$ solving the $p$ noisy function satisfies~\ref{eq:boundsxn-1} with probability $1-\delta$.
\end{theorem}
%%%%  	END THEOREM EXP   	%%%%%%%

%%%%  	PROOF THEOREM EXP   	%%%%%%%
\begin{proof}
We will prove that the probability of a misranking is small, provided that there are enough evaluations per search point. Let $x$, $y \in \dom$ be two search points output by $\mulambdaESrexp$. A misranking occurs when the real fitness values and the noisy fitness values are ordered in ``contradictory'' way on the fitness space. \nnew{That is to say, the expressions in~\ref{eq:misranking1} and~\ref{eq:misranking2} occur simultaneously.}
\begin{align}
\label{eq:misranking1} F(x) \leq F(y) &\iff ||x||^p  \leq ||y||^p, \\
\label{eq:misranking2} f(x) \geq f(y) &\iff ||x||^p+z_x  \geq ||y||^p +z_y\ . 
\end{align}
%\begin{align*}
%F(x) \leq F(y) \iff ||x||^p & \leq ||y||^p \qquad \text{and} & f(x) \geq f(y) \iff ||x||^p+z_x & \geq ||y||^p +z_y \text{ simulteneaously.}
%\end{align*}
\nnew{The misranking} happens in two situations. One, when $x$ and $y$ have very similar fitness values, so even a small amount of noise can invert their real order on the fitness space. Two,  when the noise is so large that it also alters the order. 
\\
We start by bounding the probability of the fitness to be too similar. Consider $\delta_t = \exp (-\gamma t)$ for some fixed $\gamma>0$ and $x$,$y$ be two search points at iteration $t+1$.  Define $p^f_t = \P ( \left| || x ||^p - || y ||^p \right| \leq \delta_t)$.
%\begin{equation}
%p^f_t = \P ( \left| || x ||^p - || y ||^p \right| \leq \delta_t) \nonumber
%\end{equation}
Since $x$ and $y$ are two search points at iteration $t+1$, they are mutated offsprings of parent $x_t$. Therefore $x = x_t + \sigma z_x$ and $y = x_t + \sigma z_y$ where $z_x$ and $z_y$ represent the realization of the random variables included in the mutation that generates $x$ and $y$ respectively. \nnew{ Then,}
\begin{align}
p^f_t 	& = \P ( \left| || x_t + \sigma_t z_x ||^p - || x_t + \sigma_t z_y||^p \right| \leq \delta_t) \nonumber \\
		& = \P \left( \left| || 1 + C z_x ||^p - || 1+ C z_y||^p \right| \leq \frac{\delta_t}{|| x_t ||^p} \right) &&\text{using hypothesis~\ref{eq:scaleinv}} \nonumber \\
		& \leq \P \left( \left| || 1 + C z_x ||^p - || 1+ C z_y||^p \right| \leq \frac{\exp (-\gamma t)}{(q_x \exp (Lt) )^p}\right) && \text{using hypothesis~\ref{eq:boundsxn-1}} \nonumber\\
\label{ineq:pfit}		& \leq Mq_x^{-p} \exp ((-Lp-\gamma)t) &&\text{using $z_x,z_y \sim \gauss$.}
\end{align}

In Inequation~\ref{ineq:pfit}, $M$ represents the maximum of the density of $\left| || 1+ C z_x ||^p - || 1 + C z_y||^p \right|$. Note that $M\in \R$ is a consequence of the fact that $z_x$ and $z_y$ are i.i.d. Gaussian random variables.
\\
We can bound the probability of any pair search points to have very similar fitness value in any iteration $t$ \nnew{denoted by $P^f_t$, obtaining $P^f_t	 \leq \lambda^2 p^f_t$.}
%\begin{align*}
%P^f_t	& \leq \lambda^2 p^f_t \nonumber
%\end{align*}
\\s
Now we will bound the probability of the noise being too large. We will denote this probability by $p_t^z$. More specifically, the noise is large if the estimation of the real fitness value using the noisy fitness values is larger than  $\delta_t/2$. Given that the estimation uses $K \eta^t$ reevaluations of each point to compute a mean of the noisy fitness values of that point, we obtain:
\begin{align}
p_t^z 			&=  \P \left[ \left| \frac{\gauss}{\sqrt{K \eta^t }} \right| \geq \frac{\delta_t}{2} \right] \nonumber \\
				& = \P \left[ \gauss \geq \frac{\delta_t}{2} \sqrt{K \eta^t} \right] \nonumber \\
				& \leq \frac{4}{K} \frac{1}{\delta_t^2 \eta^t}  &&\text{using Chebyshev, Lemma~\ref{lemma:chebyshev}} \nonumber \\
\label{ineq:pnoise}	& =  \frac{4}{K} \exp ( (2\gamma - \log (\eta) ) t).
\end{align} 
\nnew{Let $P_t^z$ denote} the probability of the noise being too large for at least one offspring at any iteration $t$. \nnew{Then, $ P_t^z  \leq \lambda p_t^z $.}
%\begin{align}
% P_t^z & \leq \lambda p_t^z \nonumber
%\end{align}
\\
Using~\ref{ineq:pfit}  and~\ref{ineq:pnoise} we can bound the probability of misranking, denoted \nnew{by $P_t$, as follows:}
\begin{align}
P_t					& \leq P_t^f + P_t^z \nonumber\\
					& \leq \lambda^2 p_t^f+ \lambda p_t^z \nonumber\\
\label{ineq:misranking}	& \leq \lambda^2  M q_x^{-p} \exp ((-Lp-\gamma)t) + \lambda \frac{4}{K} \exp ( (2\gamma - \log (\eta)) t).
\end{align}
The latter is a bound for the probability of misranking at iteration $t$. If the parameters are adequate in~\ref{ineq:misranking} we can bound the probability of misranking in the whole process of the ES arbitrarily $\displaystyle{\sum_{t \geq 1} P_t^z\leq \delta }$,
%\begin{align}
%\sum_{t \geq 1} P_t^z	& \leq \delta \nonumber
%\end{align}
provided that $\gamma$, $\eta$ and $K$ are large enough.
\\
In summary, we obtain that with probability at least $1-\delta$, the algorithm $\mulambdaESrexp$ obtains exactly the same rankings as $\mulambdaES$, and therefore the sequence $x_t$ output by  $\mulambdaESrexp$ solving the $p$ noisy function maintains the property~\ref{eq:boundsxn-1} assumed for the output of $\mulambdaES$ on the noise-free setting.
\end{proof}
%%%%  	PROOF THEOREM EXP   	%%%%%%%

\begin{corollary}
\label{cor:exp}
Let $n(t)$ be the number of evaluations at the end of iteration $t$. Then, with probability at least $1-\delta$
\begin{equation}
\frac{\log (|| x_n ||)}{\log (n)} \to - \frac{ R}{\log \eta} \ .
\end{equation}
\end{corollary}
\begin{proof}
First we note that $n(t) = K \eta \frac{\eta^t -1}{\eta-1}$. Then we use~\ref{eq:boundsxn-1} and the fact that we can choose $L = R - \epsilon$ and $U = R + \epsilon$ for all $\epsilon > 0$.
\end{proof}


\section{Adaptive scale dependent reevaluation}
\label{sec:adaptreval}
In Section~\ref{sec:expreval} we have used a scale invariance assumption represented by Equation~\ref{eq:scaleinv}. This feature is not realistic, since it demands the knowledge in advance of the distance to the optimum. % \todoist{INTRO}{Speak about scale invariance, the exact assumption and what motivates it. Here we just say it has to be the distance between the point and the optimum but its not clear on the definition}. 
This section presents analogous results to the ones in Section~\ref{sec:expreval}  but only using the assumption of log-linear convergence on the noise-free case. We know that this is possible, thanks to the result in \cite{auger_convergence_2005}, and its consequences detailed in Section~\ref{subsec:noisefree}. We also change the exponential reevaluation and use an adaptive scheme, depending on the stepsize. 

\begin{theorem}
\label{thm:adapt}
Assume $(\mu,\sigma)$-ES solving the sphere function satisfies 
\begin{enumerate}
\item For some  $L,U >0$, for any $\delta>0$ $\exists q_x,Q_x$ such that with probability $1-\delta/2$ for all $t$ sufficiently large:
\begin{align}
\label{eq:boundsxn-2} q_x \exp (L t) &  \leq || x_t || \leq Q_x \exp (U t) \ , \\
\label{eq:boundssigman-2} q_\sigma \exp (L t)  & \leq \sigma_t \leq Q_\sigma \exp (U t) \ .\\
\end{align}
\item Assume that the number of reevaluations per iteration is 
\begin{align}
%\label{eq:revalsadapt}
K \left( \frac1{\sigma_t}\right)^\eta \ .\nonumber
\end{align}
\end{enumerate}
Then, for any $\delta>0$, there is $K_0,\eta_0 > 0$ such that for all $K \geq K_0$, $\eta \geq \eta_0$, the points $x_t$ output by $r_{K\eta}^{\text{adap}}$-$(\mu,\lambda)$-ES with bounded density mutation 
%\todoist{CONG}{Here i call it bounded density mutation, it has to be the same when i present mu lambda ES en la intro } 
solving the $p$ noisy function satisfy~\ref{eq:boundsxn-2} with probability $1-\delta$ 
\end{theorem}

\begin{proof}
The proof is very similar to the one of Theorem~\ref{thm:exp}. We only need to adapt some steps of the reasoning. 
\\
We do not have here scale invariance or the distribution of the random variable associated to the mutation. Therefore:
\begin{align}
p^f_t 	& = \P ( \left| || x_t + \sigma_t z_x ||^p - || x_t + \sigma_t z_y||^p \right| \leq \delta_t) 
\nonumber \\
		& = \P \left( \left| || 1 + C_t z_x ||^p - || 1+ C_t z_y||^p \right| \leq \frac{\delta_t}{|| x_t ||^p} \right) \nonumber \\
		& \leq \frac{1}{C_t^d} M q_x^{-p} \exp ((-Lp - \gamma)t) && \text{using Lemma~\ref{lemma:trans},} \nonumber
\end{align}
where $C_t = \sigma_t / || x_t|| >0$ and $M$ is the maximum of the density of $\left| || 1 + C_t z_x ||^p - || 1+ C_t z_y||^p \right|$. We know $M$ is bounded because of the hypothesis on the density of the mutation random variable.
\\
We will prove now that the number of function evaluations for each iteration is larger than in Theorem~\ref{thm:exp}. We have that,
\begin{align}
K \left( \frac1{\sigma_t}\right)^\eta 	&\geq K \left( \frac1{Q_\sigma}\right)^\eta \exp (U\eta t) && \text{using hypothesis~\ref{eq:boundssigman-2}} \nonumber\\
							& \geq K' \eta'^t && \text{if $K$ and $\eta$ are large enough} \ , \nonumber
\end{align}
with $K'$ and $\eta'$ some (fixed) parametrization of the exponential reevaluation scheme. Therefore, we obtain the same results as in Theorem~\ref{thm:exp}.
\end{proof}

\begin{corollary}
Let $n=n(t)$ be the number of evaluations at the end of iteration $t$. Then with probability at least $1-\delta$
\begin{equation}
\frac{\log (|| x_n ||)}{\log (n)} \to - \frac{1}{ \eta} \ .
\end{equation}
\end{corollary}
\begin{proof}
Similar to the Corollary~\ref{cor:exp}, we compute the amount of evaluations $n(t)$. We obtain $n(t) = K \left( \frac{1}{Q_\sigma}\right)^\eta \exp(U\eta) \frac{\exp (U\eta t) - 1}{\exp(U\eta) - 1}$ and use~\ref{eq:boundsxn-2} to get the result.
\end{proof}

\section{Polynomial: experimental work}
We plot in figure~\ref{fig:noise} examples of the noisy fitness function for $p=1,2,3$ and $4$. \new{We exhibit one simulation of the deterministic function in red and one simulation of the noisy version of the function in green. This shows the difficulty we may have to find the real optimum if we have access only to the noisy evaluations of the points.}
\begin{figure}[H]
%%%%in MATLAB: plot_function_p
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[trim={2.5cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/fpnoisy1.pdf}
		\caption{p=1}
		\label{fig:p1}  
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
	%\fbox{ 
		\includegraphics[trim={2.5cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/fpnoisy2.pdf} 
		%}
		\caption{p=2}
		\label{fig:p2}  
	\end{subfigure}
	\\
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[trim={2.5cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/fpnoisy3.pdf}
		\caption{p=3}
		\label{fig:p3}  
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
	%\fbox{ 
		\includegraphics[trim={2.5cm 7cm 2cm 7cm},clip,width=1\textwidth]{XPS/fpnoisy4.pdf} 
		%}
		\caption{p=4}
		\label{fig:p4}  
	\end{subfigure}
	\caption{\label{fig:noise} Fitness function with and without noise. Dimension $d=1$}
\end{figure}
In this section we show experiments using a polynomial reevaluation. Algorithm~\ref{alg:mulambdaSAESR} is the precise pseudo algorithm used for the experiments (based on \cite{auger_theory_2011}) and the results are figure~\ref{fig:exps_poly}. \new{We plot the logarithm of the number of evaluations in the x-axis and the logarithm of the Simple Regret in the y-axis. We observe approximate linear behaviour for the log-log scale. This is the same convergence ordered obtained theoretically for the exponential and adaptive schemes in previous sections.}
\\

\begin{algorithm}[H]
\caption{$(\mu,\lambda)$-$\sigma$SA-ES algorithm with polynomial reevaluation $r_t = K t^\eta$}
\label{alg:mulambdaSAESR}
\begin{algorithmic}[1]
\State Given $d$ dimension, $\lambda \geq 5d$, $\mu \approx \lambda/4 \in \N$, $\tau \approx 1/ \sqrt{d}$, $\tau_c \approx 1/n^{1/4}$ %OJOOOOOOO NOT the same recommendation params
%\State GIVEN $d$ dimension, $\lambda = \rceil d \sqrt{d}\lceil$, $\mu =\min (d, \lceil \lambda/4 \lceil ) \in \N$, $\tau \approx 1/ \sqrt{d}$, $\tau_c \approx 1/n^{1/4}$
\Ensure Parent $x_1 \in \dom$, $\sigma_1 \in \R^{d}_{+}$, $t=1$   
\While{termination criterion not reached}  
   	\For{$j = 1, \ldots, \lambda$}
		\State $\phi_j = \tau \gauss (0,1)$
		\State $\Phi_j = \tau_c \gauss (0, \identity)$
		\State $z_j = \gauss(0,\identity)$
		\State Step-size $\sigma_{t,j} = \sigma \cdot \exp (\Phi_j) \exp (\phi_j)$ \Comment{Mutation}
		\State Offsprings $x_{t,j} = x_t + \sigma_{t,j} \cdot z_j$ \Comment{Mutation}
		\State Evaluate fitness $r_t$ times and average $y_{t,j} = \frac{1}{r_t}\sum_{i=1}^{r_t} f(x_{t,j},\omega_i)$ \Comment{Evaluation}
	\EndFor
	\State Select from the $\mu$ best search points with largest fitness values $P_\lambda = \{ (x_{t,j},\sigma_j,y_j): 1\leq j \leq \lambda\}$  \Comment{Selection}
	\State $\sigma_t = \frac1\mu \sum_{\sigma_j \in P} $ \Comment{Recombination}
	\State $x_t = \frac1\mu \sum_{x_{t,j} \in P} $ \Comment{Recombination}
	\State $t = t+1$
   \EndWhile
\end{algorithmic}
\end{algorithm}
We exhibit in table~\ref{table:poly} a summary of the parameters of the experiments and the corresponding results. \new{Each experiment includes 20 runs and we plot the average over the runs. To obtain the convergence rate (or slope) of the Simple Regret we only consider the latest evaluations. Therefore the aproximation of the convergence will represent a better estimate of  the asymptotic behaviour}. To choose $\mu$ and $\lambda$ we use the recommendations on \cite{auger_theory_2011}: $\lambda = 5d$ and $\mu = \lceil \lambda/4 \rceil$. 
\begin{table}[H]
%%%IN MATLAB: plot_mulambdaES_2013
\centering
%\scriptsize
\label{table:resultsPOLY}
	\begin{tabular}{lllllllll}
	\toprule
	Type 			& $K$	&$\eta$  	&$p$		&$\dim$	&$\lambda$	&$\mu$	&$\mathbf{ s(SR) }$\\
	\midrule
	Experiment (a) 		& 2		&1  		&2 		&2		&10			&3	&-0.2126\\
	Experiment (b) 		& 2		&2  		&2 		&2		&10			&3	&-0.3267\\
	Experiment (c) 		& 2		&1  		&3 		&2		&10			&3	&-0.1910\\
	Experiment (d) 		& 2		&2  		&3 		&2		&10			&3	&-0.3058\\
	Experiment (e) 		& 2		&1  		&4 		&2		&10			&3	&-0.1404\\
	Experiment (f) 		& 2		&2  		&4 		&2		&10			&3	&-0.2829\\
	\bottomrule
	\end{tabular}
\caption{\label{table:poly}Parameters and result of the experiments with polynomial reevaluation scheme}
\end{table}
We observe that $s(SR)$ is usually smaller than $1/(2p)$, which is slightly better than the results of \cite{rolet_bandit-based_2010}, but this effect may be due to a non-asymptotic effect. We also observe that the use of greater $\eta$ turns out to imply better results. Not presented here are results with $\eta=0$, with an unsurprising poor performance.
\begin{figure}
%%%%in MATLAB: plot_function_p
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[trim={1cm 0.7cm 0cm 0cm},clip,width=1\textwidth]{XPS/poly2_2.pdf}
		\caption{}
		\label{fig:p1}  
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
	%\fbox{ 
		\includegraphics[trim={1cm 0.7cm 0cm 0cm},clip,width=1\textwidth]{XPS/poly2_1.pdf} 
		%}
		\caption{}
		\label{fig:p2}  
	\end{subfigure}
	\\
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[trim={1cm 0.5cm 0cm 0cm},clip,width=1\textwidth]{XPS/poly3_2.pdf}
		\caption{}
		\label{fig:p3}  
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
	%\fbox{ 
		\includegraphics[trim={1cm 0.5cm 0cm 0cm},clip,width=1\textwidth]{XPS/poly3_1.pdf} 
		%}
		\caption{}
		\label{fig:p4}  
	\end{subfigure}
	\\
		\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[trim={1cm 0.7cm 0cm 0cm},clip,width=1\textwidth]{XPS/poly4_2.pdf}
		\caption{}
		\label{fig:p1}  
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
	%\fbox{ 
		\includegraphics[trim={1cm 0.7cm 0cm 0cm},clip,width=1\textwidth]{XPS/poly4_1.pdf} 
		%}
		\caption{}
		\label{fig:f}  
	\end{subfigure}
	\caption{\label{fig:exps_poly} Results of experiments with polynomial reevaluation scheme. The x-axis represents $\log (n)$ and the y-axis represents $\log (SR_n)$. We observe a linear convergence on the log-log scale for the relationship between iterations and Simple Regret.}
\end{figure}

%\section{Adaptive: experimental work}
%We exhibit experimental proof involving the adaptive scheme, theoretically analyzed in section (\ref{sec:adaptreval}). 

\section{Conclusion}

We have studied the convergence behaviour of $\mulambdaES$ adapted to handle noise by reevaluation of the fitness function. We delivered a theoretical analysis of the order of convergence for the Simple Regret on two types of reevaluation: exponential and adaptive. In addition, an experimental analysis on other type of reevaluation: polynomial. The reevaluation schemes work depending on the iteration as exposed in Table~\ref{table:revalschemes}. 
\\
The theoretical analysis yields a log-log order of convergence: the logarithm of the Simple Regret scales linearly with the logarithm of the evaluations. In the case of the exponential reevaluation scheme, we use the assumption of scale invariance to obtain the results. These assumption is popular in theoretical analysis of ES but it is not realistic.
\\
We obtain also the same order of convergence for the adaptive reevaluation scheme. The adaptation uses the step-size to compute the number of evaluations necessary at each iteration.%: if the step-size is small we will need less reevaluations, because we are close to the optimum. 
\\ 
The experimental analysis of the polynomial reevaluation scheme exhibit a similar result on the order of convergence as the theoretical analysis summarised above. We have the advantage to be able to observe also the convergence rate in this experiments. The polynomial scheme is convenient and it shows promising results experimentally, nonetheless we do not show theoretic results.
\\

%\todoist{THEO}{Check the part uniform rates}


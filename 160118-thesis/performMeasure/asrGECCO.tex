
\chapter{Approximations of Simple Regret: Inconsistent Performance}

\label{chapter:asr}
This chapter drifts apart from the analysis of specific problems/algorithms. Instead, we focus on the \emph{way} to measure the performance of algorithms. We present the consequences of using approximated performances measures to measure the performance of noisy optimization algorithms. %\todoist{STYLE}{check the reviews gecco}\
The performance measure involves the ``quality'' of the recommendation output by the algorithm. We can use the ``Regret'' to measure the quality. The Regret accounts for the loss of choosing the recommendation output by the algorithm instead of the real optimum. In other words, the regret is the difference, over the  codomain, of the recommendation and the optimum. 

The most natural way to measure the quality of the recommendation is to use the Simple Regret. It is defined as the difference between the recommendation at time $t$ and the real optimum on the image space. But some optimization algorithms also produce search points, where the function will be evaluated in order to explore the domain. The search points can be exactly the recommendations, but not necessarily.  A particular problem appears when we cannot distinguish the recommendation and search points. In that case we can imagine that the performance can be measured by looking at difference between the search points at time $t$ and the real optimum on the image space. We will see that the use of this performance measure, which we will call Approximate Simple Regret, can be misleading and return contradictory results. 

Even if we have access to the separation between recommendation and search points, noisy optimization problems present another problem to evaluate the performance of an algorithm in practice. Since we obtain noisy evaluations of points, we cannot ensure to have chosen the best point to be recommended at iteration $t$. Therefore, the Simple Regret at time $t$ can be larger (and therefore worse) that the Simple Regret at time $t+1$. In that case we cannot evaluate the algorithms with a stop criterion of first hitting time. We will see that another approximation, called Robust Simple Regret, non-increasing and using recommendation points cannot solve the contradictions. 

We analyze the use of Simple Regret, Robust Simple Regret and Approximate Simple Regret. The two latter aim to emulate the behaviour of Simple Regret. We show that they lead to incompatible performance evaluations of the same algorithms over the same class of functions. This will be shown by analysing the convergence of each Regret using the noisy sphere function and specific algorithms.
\\
 The results presented are based on the conference publication \cite{astete-morales_analysis_2016}. This is a joint work with Marie-Liesse Cauwet (TAO, INRIA Saclay) and Olivier Teytaud (TAO, INRIA Saclay, now Google).
\section{Context}

To evaluate the use of the regrets to measure the performance of algorithms, we will focus on one simple continuous noisy problem: the sphere function $F$ with additive noise.  The noisy function $f$ that is what the algorithm see is defined by:

\begin{equation}
\begin{aligned}
f: \R^d \times \Omega & \to   \R  \\
  (x,\omega) &\mapsto  F(x) + \omega  = ||x-x^*||^2 + \omega \ , \nonumber
\end{aligned}
%\label{eq:noisysphere}
\end{equation}
where $\omega \sim \gauss(0,1)$. The objective of the noisy optimization algorithms is to find $\hat x$ such that $\E_\omega f(\hat x, \omega)$ is approximately minimum. Note that the expectation is only over $\omega$, it does not include the possible randomization of the algorithm. 
%\todoist{THEORY}{all noisy optimization is like this? are there other ways to consider the expectation? -review gecco -check document remi munos, bandit simple regret or convex opt}

%\begin{figure}[H]
%	\centering
%		\includegraphics[trim={3cm 7cm 2cm 7cm},clip,width=0.5\textwidth]{XPS/spherewithnoise.pdf}
%		\caption{Sphere with noise}
%		\label{fig:spherewithnoise}  
%\end{figure}

\subsection{Simple Regret and its approximations}
The Simple Regret ($SR$) is defined at time $t$ by
\begin{equation}
SR_t = \E_\omega \left[ f(\hat x_t,\omega) - f(x^*,\omega) \right]  = F(\hat x_t) - F(x^*) \ .\nonumber
\end{equation}
The $SR$ is a way to measure the \emph{precision} of the algorithm, by verifying that the image of the recommendation $\hat x_t$ is close to the image of the real optimum $x^*$.  The evaluations of $f$ that the algorithm uses to output a recommendation are noisy. Therefore, it is not sure that the sequence $SR_t$ is non-increasing. The algorithm may output a recommendation in time $t$ that is much better than the one of the $t+1$ and drop it because of the noisy evaluation of the good solution said that it was a bad one. The first hitting time (FHT) criterion cannot be used with the $SR$. 

The Approximate Simple Regret ($ASR$) also aims to measure the performance of the algorithm but using only the evaluated points of the domain. That is is, it uses the search points. But taking into consideration the best search points with regards to the distance to the optimum on the codomain. $ASR$ is by definition non-increasing. 
\begin{equation}
ASR_t = \min_{\tau \leq t}  F( x_{\tau}) - F(x^*) \ . 
\end{equation}
%\todoist{place}{I dont know where to say that the inspiration is the COCO platform. They use ASR and RSR was suggested on the mailing list }
We also discuss the Robust Simple Regret ($RSR$), which incorporates recommendation points and it evaluates how good are they over several iterations of the algorithm. 
\begin{equation}
RSR_t = \min_{t' \leq t}  \left( \max_{ t'-g(t') < \tau < t' }F( \hat x_{\tau}) - F(x^*) \right) \ .
\end{equation}
Essentially the $RSR$ will acknowledge the best $SR$ since the start of the run, and that is also maintained as the best during $g(t')$ consecutive instants.  
%\todoist{DEF}{Ojo,  tener cuidado con t, iteration, evaluation. Definirlo bien en la intro y luego revisar que todo sea consistente.}
We will assume that $g$ is a polylogarithmic function. That implies that $g$ is ``large enough'' so that the recommendation is actually being the best for several instants but not so large that we have to have $t$ too big before acknowledge a good recommendation. The definition of $RSR$ suggests that it will outplay $ASR$ for two reasons. The first is that it uses the recommendations output by the algorithm. The second is that it checks that the recommendation is good over several iterations. In addition it is also non-increasing. We will see that even these advantages are not enough to make it a satisfactory approximation of Simple Regret. 
To measure the convergence rate of algorithms we will use the slope of each regret. 


\section{Random Search Heuristics: $ASR$ overestimates $SR$}
\label{sec:asrEA}
We include here two algorithms: Random Search and Evolution  Strategies. For the latter we consider both Evolution Strategies without and with reevaluation. Check Chapter~\ref{chapter:convES} for details on the modification of Evolution Strategies to handle noisy evaluations.

\subsection{Random Search}
We consider Random Search as in Algorithm~\ref{alg:rs}. We consider that the search distribution of Random Search works as follows. Each search point is selected once and only once from a uniform distribution \nnew{over $\dom = [0,1]^d$. We assume the optimum $x^*$ belongs to $\dom$}. The recommendation point is the best search point so far. 
\begin{property}
$s(ASR)=O(-2/d)$ a.s. for Random Search on the noisy sphere function.
\end{property}
\begin{proof}
%\nnew{From the work in  \cite{deheuvels_strong_1983} we know that among $i$ points,  generated independent and uniformly over $\dom = [ 0,1 ]^d$, the closest to the optimum is almost surely at distance} 
\nnew{From the work in  \cite{deheuvels_strong_1983} we know that the point that is closest to the optimum $x^*$ is almost surely at distance $O(t^{-1/d})$ within a logarithmic factor, considering a sample of points of size $t$. Hence we obtain the result using the definition of ASR.}
%We generate $i$ search points independently and uniformly over $\dom = [ 0,1 ]^d$. We know from \cite{deheuvels_strong_1983} that the search point that is closest to the optimum $x^*$ is at distance $\Theta (n^{-1/d})$, within a logarithmic factor. Hence the result by definition of $ASR$. 
\end{proof}
\nnew{We provide in the Appendix, Section~\ref{sec:appendixRSxps}, experiments that suggest that the Random Search in fact converges for the noisy sphere. Even more, according to the experiments the upper bound might be reached.}

In the case of $SR$, we prove that $s(SR)$ is not negative. Therefore, the convergence measured by $ASR$ overestimates the real convergence measured by $SR$. 
\begin{theorem}
\label{thm:randomsearch}
For all $\beta>0$, $\E [SR_t] \notin O(t^{-\beta})$ for Random Search on the noisy sphere function.
\end{theorem}
For the formal proof we refer the reader to the Appendix, Section~\ref{sec:appendixRS}. Roughly speaking, the argument is that with probability non-zero, we select a recommendation point that is not the one with the best fitness.

\subsection{Evolution Strategies}

 The work from \cite{arnold_general_2006} shows that an ES without any adaptation to noise stagnates around some step-size and at some distance from the optimum. \nnew{We can also find experimental results for ES solving noisy functions in \cite{beyer_mutate_1998}: they stagnate at some step-size and some distance to the optimum. These divergence results suggest that the ES act only as a more sophisticated version of Random Search for the optimization of noisy functions. } We propose a conjecture on the convergence rates for ES without a noise treatment.

\begin{conjecture}
The convergence rate for  regrets  of Evolution Strategies are equal to the convergence rates for Random Search on the noisy sphere function. %That is,
%\begin{align*}
%s(SR)&=0 \ , 	&s(ASR)&=-2/d	\\ % \ ,  &s(RSR)&=-2/d \ .\\
%\end{align*}
\end{conjecture}
Now we consider an ES with noise treatment: reevaluation of search points. We will denote it by ES-r, where r represents the reevaluation scheme. We know by  Chapter~\ref{chapter:convES}  
 that ES-r converge for the $SR$ if they have the property of step size scaling as the distance to the optimum.  We also know from the work on Chapter~\ref{chapter:lowerboundES} that
 %\cite{astete-morales_evolution_2015} 
 they cannot be faster than $s(SR)=-1/2$. And from experiments, we can see that they seem to reach $s(SR)=-1/2$. For more details on the convergence of ES-r and experiments go to Chapter~\ref{chapter:convES}. For more details on the lower bound for the convergence of ES-r go to Chapter~\ref{chapter:lowerboundES}.
Therefore we propose:
\begin{conjecture}
\label{conj:srES-r}
$s(SR)=-1/2$ with probability $1-\delta$ for ES-r on the noisy sphere function.
\end{conjecture}
The conjecture~\ref{conj:srES-r} is valid for some ES that satisfy the conditions on detailed on Chapter~\ref{chapter:convES}. In other words, ES must have a reevaluation strategy, represented by $r$. The strategy can be either exponential or polynomial reevaluation. And also the algorithm must satisfy the condition over the step-size scaling as the distance to the optimum (see condition~\ref{eq:scaleinv}). 
Using this conjecture, we can prove that $s(ASR)$ is strictly better. We modify the algorithm ES-r (Algorithm~\ref{alg:esasr})and to obtain $s(ASR)=-1/2-2/d$. Let us present the modification of the algorithm, called MES-r in Algorithm~\ref{alg:mesasr}. We will modify the stage of generation by producing additional offsprings. And we modify the stage of evaluation by evaluating these additional offsprings. Nonetheless, the additional offsprings will not be taken into consideration for creating the recommended point.

\begin{theorem}
\label{theo:asrMES}
Let $0 < \delta <1$. Assume that
 \begin{align} || \hat x_n || & = \Theta (\sigma_n) \label{eq:xsigma} \ ,  \\
  \frac{( ||\hat x_n|| )}{log(n)} & \xrightarrow{n\to\infty} - \frac12 \text{ with probability } 1-\delta \ . \label{eq:loglog}\end{align}
Then $s(ASR)=-1/2 - 2/d$ with probability $1-\delta$ for MES-r on the noisy sphere function.
\end{theorem}
\begin{remark}
Just as in Conjecture~\ref{conj:srES-r}, we need to assume the step-size scaling as the distance to the optimum. Note that we fix the reevaluation strategy as exponential, on Algorithm~\ref{alg:mesasr}. We assume also  in the proof of  Theorem~\ref{theo:asrMES} that Conjecture~\ref{conj:srES-r} is true.
\end{remark}

\begin{minipage}{0.5\textwidth}
\centering
\begin{algorithm}[H]
\caption{\label{alg:esasr} { ES-r}}
\begin{algorithmic}[1] %\scriptsize
	\State{{\bf Input:} $\mu$, $\sigma$ and $r$}
	\State{{\bf Initialize:} Parent $\hat x$, stepsize $\sigma$}
	\State{{\bf Initialize:} $ i \leftarrow1$}
	\While{not terminate}
	\For{$ j \in \{1, \ldots, \lambda \}  $}
		\State{Mutation: $x_j \leftarrow \hat x+ \sigma \gauss$}
		\State{Evaluation: $y_j \leftarrow \frac1{r(i)} \sum_{j=1}^{r(i)}  f(x_i,\omega_i)$ }
		\EndFor
		\State{Selection: select $\mu$ best out of $(x_i)_{1}^\lambda$}
		\State{Update $\hat x$ using $\mu$ offsprings and $\sigma$}
		\State{$i\leftarrow i+1$}
	\EndWhile \\
	\Return{$\hat{x}$}
	\Statex
	\Statex
	\Statex
	\Statex
\end{algorithmic}
\end{algorithm}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\centering
\begin{algorithm}[H]
\caption{\label{alg:mesasr} { MES-r}}
\begin{algorithmic}[1]%\scriptsize
	\State{{\bf Input:} $\mu$, $\sigma$ and $r$}
	\State{{\bf Initialize:} Parent $\hat x$, stepsize $\sigma$}
	\State{{\bf Initialize:} $ i \leftarrow1$}
	\While{not terminate}
	\For{$ j \in \{1, \ldots, \lambda \}  $}
		\State{Mutation: $x_j \leftarrow \hat x+ \sigma \gauss$ }
		\State{Evaluation: $y_j \leftarrow \frac1{r(i)} \sum_{j=1}^{r(i)}  f(x_i,\omega_i)$}
		\EndFor
		\State{Selection: select $\mu$ best out of $(x_i)_{1}^\lambda$}
		\State{Update $\hat x$ using $\mu$ offsprings and $\sigma$}
	\For{$ j \in \{1, \ldots, r(i) \}  $}
		\State{Mutation: $x'_j \leftarrow \hat x+ \sigma \gauss$}
		\State{Evaluation: $y'_j \leftarrow  f(x'_i,\omega_i)$ }
		\EndFor
		\State{$i\leftarrow i+1$}
	\EndWhile \\
	\Return{$\hat{x}$}
\end{algorithmic}
\end{algorithm}
\end{minipage}

\begin{proof}
We have $r_n = K \eta^n$.
In this proof we will index the recommendation and search points  by the number of \emph{iterations} instead of the number of evaluations. For ES-r, the recommendation point of the iteration $n$ is  the corresponding center of the offspring distribution $\tilde x_n$. The step-size $\sigma_n$ corresponds to the standard deviation of the offspring distribution. The search points of iteration $n$ are the $\lambda$ offsprings produced in the iteration, denoted $\{ x_n^{(i)}: i=1,\ldots,\lambda \}$.


We define $e_n$ the number of evaluations until the iteration $n$. From Algorithm~\ref{alg:mesasr} 
we have $e_n=\lambda \sum_{i=1}^{n} r_i$ for an ES with reevaluation $r$. 
By hypothesis (Equation~\ref{eq:loglog}) we know the convergence  of the sequence $S_n$ defined as the logarithm of the recommendation points, indexed by the number evaluations, divided by the logarithm the number of evaluations. Therefore, the sequence $\mathcal{S}_n = \log (||x_n||) /\log (e_n)$ is a subsequence of $S_n$, hence convergent to the same limit, with the same probability. The relation in Equation~\ref{eq:xsigma} also remains the same after the index modification. From these facts we can immediately conclude that $\exists \quad n_0$ such that
\begin{equation}\label{ensigma}
\sigma_{n}=\Theta(e_n^{-1/4}) \mbox{ for $n\geq n_0.$}
\end{equation}
For the MES-r algorithm, the $ASR_n$ is defined as:
\begin{equation}
{ASR_n =\min_{m\leq n} \min_{1\leq i \leq \lambda+r_m}\|{x'}_{m}^{(i)}\|^{2}} \ ,
\end{equation}
where $\{ {x'}^{(i)}_n : i= 1,\ldots,\lambda+r_m \}$ considers all the search points at iteration $n$. Both the ``real'' and the ``fake'' ones. We will find a bound for $ASR_n$, which will lead us to the convergence rate of the $ASR$ for the MES-r algorithm.
 
Let $p_x$ be the probability density at point $x$ of the offsprings in iteration $n$. Therefore it is the probability density of a Gaussian centered at $\tilde{x}_n$ and with variance $\sigma_n^2$. At the origin:
\begin{align}
p_0&=\frac{1}{(2\pi)^{d/2}\sigma_{n}^{d}}\exp\left\{-\frac12 \frac{(-\tilde{x}_n)^{T}(-\tilde{x}_n)}{\sigma_{n}^2}\right\}\ , \nonumber\\
     &=\Theta(\sigma_{n}^{-d}) \ , \nonumber \\ %~\mbox{by Eq.~\ref{xsigma}}\nonumber\\
     &=\Theta(e_n^{d/4}) \ .%~\mbox{by Eq.~\ref{ensigma}}
     \label{densitybound}
\end{align}
Now, at iteration $n$, \nnew{we can bound} the probability to have at least one offspring with norm less than $\epsilon>0$
\begin{eqnarray*}
\P(\exists i : \|{x'}_{n}^{(i)}\|\leq \epsilon)&\leq (\lambda + r_n )\P(\|{x'}_{n}^{(i)}\| \leq \epsilon) \ , \\
				      			    &\leq (\lambda +r_n) \int_{\|x\| \leq \epsilon}dp_x \ .
\end{eqnarray*}
By Equation~\ref{densitybound}, 
$$\P(\exists i | \|{x'}_{n}^{(i)}\|\leq \epsilon)=\Theta(r_n\cdot e_n^{d/4} \epsilon^{d})=\Theta(1) \ ,$$
 if $\epsilon = \Theta(  e_n^{-1/4} \cdot r_n^{-1/d} )$.
 Therefore we obtain:
\begin{align*}
ASR_n &\leq \min_{1\leq i \leq \lambda+r_n} \|{x'}_{n}^{(i)}\|^{2} \ , \\
			&\leq\epsilon^{2} \ , \\
			&=O(e_n^{-1/2} \cdot r_n^{-2/d}) \ , \\
			&=O(e_n^{-1/2-2/d}) \ .
\end{align*}
Since $e_n=(1+\lambda)\sum_{i=1}^{n} r_{i}=(1+\lambda)\cdot R\sum_{i=1}^{n}\zeta^{i}=\Theta(r_n)$, we have the result: $$s(ASR)\leq -1/2-2/d \ .$$
\end{proof}

\section{Noisy Linesearch Algorithms: $ASR$ underestimates $SR$}
\label{sec:asrGRADIENT}

We analyze two algorithms that use estimated gradient to find the optimum of noisy functions, both presented in Section~\ref{sec:linesearchstochastic}. The first one is Shamir that uses a one point technique to estimate the gradient. The other one is Fabian, that uses finite differences to estimate the gradient. They have both proved to be optimal for $SR$. $s(SR)=-1$ in expectation for Shamir over quadratic functions and $s(SR)=-1$ approximately and asymptotically for limit values of parameters for Fabian for smoothly enough functions.

\subsection{Shamir}
The following result is a direct consequence from a wider result for more general functions proved in \cite{shamir_complexity_2013}. 
\begin{theorem}
\label{thm:shamirasr}
$s(SR)=-1$ in expectation for Shamir algorithm on the noisy sphere function.
\end{theorem}

The work on \cite{shamir_complexity_2013} proves results in expectation. But if we assume the same results occur for almost sure convergence, then we obtain immediately that the $ASR$ underestimates the convergence of the Shamir algorithm.

\begin{theorem}
\label{thm:asrshamir}
If Theorem~\ref{thm:shamirasr} is also valid a.s. then 
$s(ASR)=0$ for Shamir on noisy sphere function. 
\end{theorem}

\begin{proof}
Assume that the recommendations of Shamir $\hat x_i$ converge a.s. to the optimum $x^*$. By definition of the Shamir algorithm, the sequence of search points is at a constant distance $\eta$ from $x^*$. Therefore, $\min_{j \in \{1,\ldots, i\}} || x_j - x^*||$ is lower bounded by $\eta$. Which implies that $s(ASR) = 0$ \nnew{almost surely.}
\end{proof}
Note that the consequences of Theorem~\ref{thm:asrshamir} apply as long as the problem has a unique optimum, the sequences of recommendations of the algorithm converge a.s. to the optimum and the sequence of search points is such that the distance between the search points and the optimum is always constant. Therefore the result is wider and not only for the Shamir algorithm. Even so, the result for Shamir algorithm is only valid if we assume the conjecture that the results in Shamir's work are also valid for almost sure convergence.

%\begin{conjecture}
%$s(ASR)=0$ a.s. for Shamir algorithm
%\end{conjecture}

\subsection{Fabian}
The following result in Theorem~\ref{thm:SRfabian} comes from the results proved in \cite{fabian_stochastic_1967}. 
%\begin{theorem}
%s(SR)= -1 for Fabian algorithm with limit values of parameters on the noisy sphere function
%\end{theorem}
\begin{theorem}
\label{thm:SRfabian}
Let $s$ be an even positive integer and $F$ be a function {${(s+1)}$-times differentiable} in the neighborhood of its optimum $x^*$. Assume that its Hessian and its $(s+1)^{th}$ derivative are bounded in norm. Assume that the parameters given in input of Algorithm~\ref{alg:fabian} satisfy: $a>0$, $c>0$, $\alpha= 1$, $0<\gamma<1/2$ and $2\lambda_0 a >\beta_0$ where $\lambda_0$ is the smallest eigenvalue of the Hessian. Let ${\beta_0=\min\left(2 s \gamma, 1 - 2 \gamma \right)}$. Then, a.s.:
\begin{equation}\label{fabiantropfort}
n^{\beta}(\tilde{x}_n-x^*) \rightarrow 0\ \forall\ \beta <\beta_0/2 \ .
\end{equation}
In particular, when $F$ is smooth enough, we get ${s(SR)=-2\beta}$.
\end{theorem}

We prove here the convergence rate for $ASR$ in the form of the following theorem:


\begin{theorem}
\label{thm:asrfabian}
$s(ASR)=-\min(2 \beta ,2 \gamma) \ .$
\end{theorem}
The detail of the proof is in Appendix~\ref{sec:appendixfabian}. We can deduce directly the $s(ASR)$ using the  parameters for optimal convergence rate of $SR$, with the following Corollary:
\begin{corollary}
\label{cor:fabian}
$s(ASR) \to 0$ when $\gamma \to 0 \ .$
\end{corollary}
\nnew{In Corollary \ref{cor:fabian} the fact that $\gamma$ tends to $0$} is required to obtain the result in~\ref{thm:SRfabian}. Therefore, we obtain that Fabian proves the algorithm is optimal for $SR$. Simultaneously, we prove that the algorithm does not converge for $ASR$ using the same parametrization that yields optimal convergence for $SR$.

\subsection{Adapt algorithms}
We can adapt both Shamir and Fabian so that they evaluate the recommendation points and we will obtain $s(ASR)=-1$. All we need to do is evaluate also the recommendation points. Even if they are not used anywhere in the algorithm. This is a serious problem: adding a senseless feature such as revaluate points and not use this information help us \emph{improve} the results in terms of approximating the $SR$. But the objective of the algorithm is to \emph{reduce} the amount of function evaluations. \\
On the contrary, recall that for the analysis of MES-r we add extra evaluations and we obtain a better convergence rate for $ASR$ than for $SR$.%\todoist{COMMENT}{Esto es grave, porque agregar un feature ``estupido'', como evaluar un punto que no se usa para nada en el algoritmo permite mejorar significativamente el rendimiento del algoritmo. Queremos mejores resultados ``reales'', no falsos positivos. En este caso el falso positivo es util y ayuda a acercar la medida de rendimiento a la medida real, pero no es seguro que eso ocurra todo el tiempo, tal como cuando se usa ASR en los EA, donde se el resultado final es un falso positivo.}

\section{General Results for $SR$, $ASR$ and $RSR$}

The issue treated in this chapter is the gap between the regrets. Ideally we should have some convergence of the $ASR$ and $RSR$  to $SR$ in some way. Nonetheless, this is not the case. We will see over the  sections ~\ref{sec:asrEA} and~\ref{sec:asrGRADIENT} that $ASR$ both underestimates and overestimates the performance of algorithms, depending on the type of algorithm. In the case of Evolution Strategies without a noise treatment, they converge for $ASR$. In the case of $SR$, ES do not converge. On the contrary, for algorithms in using estimated gradients, $ASR$ does not report that they converge. When in fact, the convergence is optimal for $SR$. 
\\
For the $RSR$, we have that by definition $s(RSR) \leq s(SR)$. Therefore, $s(RSR)$ is a correct lower bound for $s(SR)$. Nonetheless, it is not a tight bound. We show that by modifying slightly the algorithm, we obtain that $s(RSR) \leq s(ASR)$ without reporting any change on $s(SR)$.  Let algorithm $A$ with search points $(x_t)_{t\geq1}$ and algorithm $A^g$ with search points $(x_t^g)_{t\geq1}$. The search points of $A^g$ are repeated search points of $A$. The definition uses a polylogarithmic function $g$ depending of the iteration $t$ as follows:
% The first $g(1)$ search points of $A_g$ are all equal to the first search point of $A$. The next $g(2)$ points of $A_g$  are all equal to the second search point of $A$ and so forth and so on. 

\begin{align*}
x^g_1 &= x_1 \ ,	& x^g_{g(1)+2} &=x_2 \ ,			&\ldots	&& x^g_{i+\sum{j=1}^{i-1} g(j)}	&=x_i \ ,\\
x^g_2 &=x_1 \ ,		& x^g_{g(1)+3} &=x_2 \ ,			&		&&\\
	  &\vdots 	& 			&\vdots 	&		&&						&\vdots\\
x^g_{g(1)+1}  &=x_1 \ ,	& x^g_{g(2)+1} &=x_2 \ , 	&\ldots	&&x^g_{i+\sum{j=1}^{i} g(j)}	&=x_i \ .\\
\end{align*}
The recommendation points of $A^g$ are defined as $\hat x^g_i := x^g_i$ for any $i$. 
%\todoist{PROOF}{check proof}

By the definition of the regrets we do not have more information about the relationship among them. We will see them in action over five algorithms, over the same noisy optimization problem. 

\section{Experiments}
%\todoist{ADD}{Can I add RSR ????}
\label{sec:experiments}
We present experimental results for part of the algorithms theoretically analyzed\footnote{In addition, the experimental results we include the algorithm $UHCMAES$, as another example of an $ES$. For more information, see ~\cite{hansen_method_2009}.}.  We will analyze the convergence rate of these algorithms. We will plot the convergence rate (or slope) of the Simple Regret for each number of function evaluations.
\\
 As in the theoretical part of this study, the function to optimize is the noisy sphere: $f(x)=\|x-x^*\|^2+ \vartheta \gauss$ where $\vartheta=0.3$ and $\gauss$ is a standard Gaussian distribution\footnote{ The choice $\vartheta=0.3$ is made only to illustrate in the experiments the effect of the regret choice in a reasonable time budget. The noise is weaker than in the case of a standard Gaussian and the algorithms can deal with it faster. The optimum $x^*$ for the experiments of each algorithm is different, which does not affect the result since the regret compares the function value on the search/recommended points and on the optimum.}. The dimension of the problem is $d=2$. The results in Figure~\ref{fig:asr} correspond to the mean of 10 runs for each of the algorithms.
 \begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[trim={1cm 0cm 1.5cm 0cm},clip,width=1\textwidth]{XPS/SR.eps}
		\caption{Simple Regret}
		\label{fig:SR} 
	\end{subfigure}
	\begin{subfigure}[b]{0.48\textwidth}
		\includegraphics[trim={1cm 0cm 1.5cm 0cm},clip,width=1\textwidth]{XPS/ASR.eps}
		\caption{Approximate Simple Regret}
		 \label{fig:ASR} 
	\end{subfigure}
	\caption{\label{fig:asr} Figure~\ref{fig:SR} presents the Slope of Simple Regret for each algorithm on the first $(1\cdot10^6)$ function evaluations. Stochastic Gradient algorithms reach $s(SR)=-1$ while the evolutionary algorithms present $s(SR)=-0.2$. Figure~\ref{fig:ASR} presents the Slope of Approximate Simple Regret. Observe that the performance of the algorithms is inverted with regards to the figure~\ref{fig:SR} : now the Stochastic Gradient algorithms have the worse performance.}
\end{figure}

\begin{table}[H]
%\scriptsize
	\begin{center}
		\begin{tabular}{rl}
		\toprule
	 		Algorithms & Set of parameters \\
	 	\midrule
		UHCMAES ~\cite{hansen_method_2009} &  $x_{\text{initial}}=1$,  $\sigma_{\text{in}}=1$ \\
		Shamir &$\epsilon=0.3$,  $\lambda=0.1$, $B=3$\\
		$(1+1)$-ES & \\
		$(1+1)$-ES-r & reevaluation$=2^n$  \\
		Fabian & s=4 $\alpha=1$,  $\gamma=0.01$	 \\
		\bottomrule
 		\end{tabular}
	\end{center}
%\caption{
\label{table:exps} %Parameters for Experiments}
\end{table}




The results in figure~\ref{fig:SR} \new{show the convergence rate for the different algorithms with regards to the $SR$}. The budget is limited to $(1\cdot10^6)$ function evaluations. We can see clearly the difference between \new{linesearch algorithms, Fabian and Shamir and $ES$}. The algorithms Fabian and Shamir achieve $s(SR)=-1$ whereas the $ES$ presented cannot do better than $s(SR)=-0.25$. The figure
~\ref{fig:ASR} shows that the use of $ASR$ changes completely the performance of the algorithms. In this case, the gradient-based algorithms are the ones with the worst performance. The results support the theoretical work (and the conjectures) presented in sections~\ref{sec:asrEA} and~\ref{sec:asrGRADIENT}.

%We present in figure  (\ref{zutoum}, see supplementary material) the results for $(1+1)$-ES using several type of reevaluation schemes. The left column corresponds to polynomial and the right corresponds to exponential. Notice that in both columns, the lines corresponding to $n^0$ and $1^n$ are equivalent and they represent the $(1+1)$-ES with no reevaluation whatsoever. We can observe the effects mention on the theoretical part. In the first place, well tuned $(1+1)$-ES can reach a slope for SR equal to $-1/2$. In second place, the $(1+1)$-ES without reevaluations has the best performance with regards to $ASR$ criterion but the worst with regards to $SR$.


\section{Conclusion}

We analyze the use of approximations of Simple Regret and how they give inconsistent results at measuring the performance of algorithms. For $ASR$ we obtain that it underestimates the performance of algorithms that estimate the gradient. This can be solved by modifying the algorithm and evaluating recommendation points so that they can be taken into account for ASR. For the case of EAs, $ASR$ overestimates their performance. There is no easy way to solve this issue by modifying the algorithms. 

$RSR$ on the other hand solves partly the issues. It is by definition a valid lower bound for the performance in terms of $SR$ for any algorithm. Unfortunately, this bound is not tight. We prove that a slight modification of the algorithm implies that $RSR$ is also a lower bound for $ASR$, without implying any modification on $SR$. 

Even though $SR$ is the natural way to measure the precision of the recommendation given by the algorithm, its non-non-increasing nature does not allow it to be used easily in practice for the performance evaluation of evolutionary algorithms. 

We have compared convergence rates with different mode of convergence. There is room for refinement of the results. 

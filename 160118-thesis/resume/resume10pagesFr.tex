\documentclass[12pt,letterpaper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage[]{textpos}
\usepackage[francais]{babel}

 \usepackage{setspace}
 \usepackage{graphicx}

\title{Evolution Artificielle pour la Robotique Collective en Environnement Ouvert \\
{\small Environment-driven Distributed Evolutionary Adaptation for Collective Robotic Systems}}

\begin{document}

\maketitle

\begin{doublespace}
Cette thèse décrit une partie du travail effectué dans le cadre du projet européen Symbrion\footnote{IP, FP7, 2008-2013}. Ce projet vise à la réalisation de tâches complexes nécessitant la coopération de multiples robots dans un cadre de robotique en essaim (au moins 100 robots opérant ensemble). De multiples problèmes sont étudiés par le projet dont: l'auto-assemblage de robots en structures complexes et l'auto-organisation d'un grand nombre de robots afin de réaliser une tâche commune. Le principal sujet d'intérêt est par conséquent les mécanismes d'auto-adaptation pour la robotique modulaire et en essaim, avec un intérêt pour des capacités de forte coordination et de coopération à l'échelle de l'essaim.

	Les difficultés rencontrées dans la réalisation de ce projet sont dues à l'utilisation de robots dans des environnements ouverts restant inconnus jusqu'à la phase de déploiement. Puisque les conditions d'opérations ne peuvent être prédites à l'avance, des algorithmes d'apprentissage en ligne doivent être utilisés pour élaborer les comportements utilisés. Lorsqu'un grand nombre de robots sont utilisés, plusieurs considérations doivent être prise en compte: capacité de communication réduite, faible mémoire, faible capacité de calcul. Par conséquent les algorithmes d'apprentissage en ligne doivent être distribués à travers l'essaim.
	
	De multiples approches ont déjà été proposées pour faire face aux problèmes posés par l'apprentissage en ligne décentralisé de comportements robotiques, parmi lesquels la robotique probabiliste, l'apprentissage par renforcement, et la robotique évolutionnaire:
	\begin{itemize}
	%TODO: c'est nouveau, il va falloire faire gaffe à la formulation	
		\item Les travaux effectués en robotique probabiliste visent la création de comportements robustes à des variations prévisibles de l'environnement. Cette approche se concentre principalement sur la phase de conception, pendant laquelle les comportements créés sont testés de façon intensive dans un large répertoire de scénarii. Les comportements ainsi conçus sont adaptables aux conditions prévues par l'ingénieur humain. Un des exemples phare de cette approche est la réalisation de voitures capables de se déplacer en toute autonomie dans un environnement urbain.
		\item L'approche apprentissage par renforcement vise à automatiser la phase de conception dans des cas où la tâche et l'environnement sont connus, mais où le problème est trop complexe pour être résolu par un ingénieur humain. Les contributions à cette approche visent à la création de méthodes de conception automatiques flexibles par rapport à la tâche et l'environnement. Grâce à cette approche il est notamment possible de concevoir des programmes pour piloter des robots avec un grand nombre de degrés de liberté pour réaliser des tâches très variées (e.g. jouer au ping-pong, faire la cuisine, jongler).
		\item Les algorithmes évolutionnaires sont utilisés pour résoudre des problèmes de type boîte noire. Il s'agit de problèmes où l'on cherche à optimiser les paramètres d'un système dont on ignore le fonctionnement. La seule mesure connue est la performance du système par rapport à la tâche à réaliser. Les méthodes évolutionnaires sont donc aussi des méthodes de conception visant l'adaptabilité à un grand nombre de tâches et d'environnements. De récents travaux ont visés l'adaptation de ces méthodes à la résolution en ligne de problèmes robotiques, ce qui soulève de multiples difficultés (e.g. bruit lors de l'évaluation, risque d'endommager les robots, contraintes matérielles fortes).
	\end{itemize}
	
	Le problème abordé dans le cadre de cette thèse est encore plus complexe puisqu'un groupe de robot est considéré (en lieu et place d'un seul et unique robot). Par conséquent, les algorithmes utilisés doivent être à même de prendre en considération les comportements émergent des interactions d'un grand nombre de robots. Ceci augmente significativement la complexité algorithmique de l'évaluation d'une solution, en plus de l'introduction de bruit. De plus, dû à la nature ouverte de l'environnement, le comportement optimal souhaité n'est pas connu. Il n'est pas possible de supposer que l'ingénieur humain ait les connaissances nécessaires pour définir les éléments indispensables aux processus d'apprentissage.
	
	Il apparaît qu'assurer l'intégrité de l'essaim (par exemple en survivant grâce à des recharges d'énergie) est nécessaire à une future adaptation pour une tâche spécifiée par l'utilisateur. Par conséquent, ce problème est placé en tant que premier élément d'une feuille de route visant à définir un ensemble d'étapes nécessaires à la réalisation d'une tâche par un groupe de robot dans un environnement ouvert:
	\begin{itemize}
 		\item Étape 1: Assurer l'intégrité de l'essaim.
 		\item Étape 2: Maintenir les robots disponibles en tant que service à l'utilisateur.
 		\item Étape 3: Réaliser la tâche définie par l'utilisateur.
	\end{itemize}
 	
 	Dans le cadre de cette thèse nous travaillons à la réalisation de l'étape 1 de cette feuille de route. Il s'agit d'un problème très difficile à résoudre, puisque aucune connaissance d'un ingénieur humain n'est utilisable. Cette difficulté n'a pas été abordée dans les travaux précédents cette thèse. Pour la résoudre, nous assumons l'hypothèse de travail suivante:

\begin{textblock}{10}[0.0,0.0](0.07,0.01)
\paragraph{Hypothèse de travail:}
Dans un cadre de robotique collective en environnement ouvert, la réalisation d'une tâche définie par l'utilisateur implique tout d'abord un comportement auto-adaptatif.
\end{textblock}
\vspace*{3cm}
 	
	Le sujet de cette thèse est la réalisation de solutions algorithmiques décentralisées pouvant garantir l'intégrité d'un essaim de robots en environnement ouvert lorsque un système robotique collectif utilise une communication locale. Le domaine de la Robotique Evolutionnaire Embarquée Distribuée s'intéresse à un cas similaire lorsque le comportement optimal est spécifié (de façon explicite ou implicite). La principale difficulté pour résoudre ce problème original est le besoin de prendre en compte l'environnement. En effet, en fonction de l'environnement courant, les robots peuvent avoir à démontrer une grande variété de comportements à l'échelle de la population comme la coopération, la spécialisation, l'altruisme, ou la division du travail. Ces comportements ne peuvent être spécifiés à l'avance puisqu'ils dépendent de l'environnement, des robots et de la tâche à effectuer. 

Résoudre une tâche adaptative dirigée par l'environnement peut-être vu comme la satisfaction de deux motivations qui peuvent être antagonistes. La première motivation est de garantir l'intégrité d'un plus grand nombre de robots possible au sein du groupe dans l'environnement courant (motivation extrinsèque). La seconde motivation est de permettre les interactions nécessaires entre les robots et l'environnement afin d'assurer le bon fonctionnement de l'algorithme (motivation intrinsèque). Il y a par conséquent un compromis entre des comportements conservateurs et des comportements explorateurs. D'un côté, les comportements conservateurs tendent à rester à un endroit et donc à satisfaire la motivation extrinsèque. Cependant ce type de comportement se fait au risque d'empêcher les interactions nécessaires à la satisfaction de la motivation intrinsèque. De plus ce type de comportement tend à empêcher la réalisation de la tâche. De l'autre côté, les comportements explorateurs permettent de tester un grand nombre d'interactions possibles entre robots et entre les robots et l'environnement. Par conséquent, ces comportements satisfont la motivation intrinsèque. Ceci ce fait au risque d'endommager le robot ou de se perdre, c'est-à-dire au risque de ne pas satisfaire la motivation extrinsèque.

Dans cette thèse nous introduisons et définissons le problème de l'Adaptation Evolutionnaire Distribuée Guidée par l'Environnement. A travers ce problème nous formalisons les points évoqués lors des paragraphes précédents: nécessité de l'auto-organisation, importance du compromis entre motivation extrinsèque et intrinsèque. Une version simplifiée de ces principes est illustrée grâce à la figure~\ref{graph:edea}. Cette figure représente l'évolution fictive d'un système EDEA pendant une génération. La première image montre le début de la simulation: chaque robot commence avec une liste vide de génomes. Les deuxième et troisième images montrent les robots en train de se déplacer (le comportement de chaque robot est contrôlé par son propre génome actif), et d'échanger des génomes lorsqu'ils sont assez proches les uns des autres. La quatrième image montre la simulation à la fin de la génération: le génome rouge s'est répandu plus fortement dans la population et a par conséquent une plus grande chance d'être sélectionné. La prochaine génération contiendra des versions légèrement mutées des génomes originaux. 

\begin{figure}%[h]
\centering
\includegraphics[width=0.8\textwidth]{edea-example.png}
\caption{\label{graph:edea} Illustration des principes de l'Adaptation Evolutionnaire Distribuée Guidée par l'Environnement.}
\end{figure}

Cet exemple montre  l'impact de l'environnement sur le processus évolutionnaire. En effet, la probabilité de sélection de chaque génome dépend uniquement du comportement résultant de son expression. Le comportement produit dépend de l'environnement  dans lequel le génome se trouve. Ce point de vue est très différent des travaux effectués dans le cadre des algorithmes évolutionnaires avec une fonction objectif, où la probabilité de sélection dépend de l'évaluation du comportement par une fonction.

Nous proposons un algorithme incorporant les mécanismes illustrés précédemment appelé \textsc{mEDEA}, pour minimal Environment-driven Distributed Evolutionary Adaptation. Comme son nom l'indique, cet algorithme implémente l'ensemble minimal de mécanismes nécessaires à la résolution de problèmes EDEA.
 
Dans cette thèse, l'algorithme \textsc{mEDEA} est utilisé pour évaluer certains aspects de l'auto-adaptation:
\begin{itemize}
	\item Nous cherchons tout d'abord à comprendre le comportement des agents à l'échelle de la population. Ceci est difficile car les nombreuses interactions entre agents et avec l'environnement (et leurs conséquences) ne peuvent être prédite. De plus la qualité d'un génome dépend du compromis entre exploration et exploitation, qui n'est pas connu à l'avance. Plus précisément les questions suivantes sont posés : est-ce que toute la population converge vers un comportement canonique ? Si non, quelles sont les différences entre les comportements ? Est-ce qu'il existe des comportements temporaires ? Est-ce que l'on observe la co-existence de deux sous-populations ayant des comportements différents ? Les expériences menées dans ce cadre permettent de valider le fonctionnement de l'algorithme, ainsi que l'influence de deux paramètres clés: le nombre de robots et la taille du rayon de communication. De plus, nos expériences montrent que majoritairement les consensus comportementaux évoluent lorsque des artefacts existent dans l'environnement. De plus différents consensus ont été observés, et dans certains cas la co-existence de deux consensus conjointement dans la population. Des expériences similaires conduites sur des robots réels (e-puck) ont permis de valider ces résultats.
	\item Nous avons ensuite évalué la capacité d'adaptation en ligne de l'algorithme face à des changements environnementaux. Cet aspect est difficile car l'apprentissage est uniquement guidé par l'environnement, c'est-à-dire par la capacité des robots à survivre et interagir dans celui-ci. Par conséquent, le moment du changement d'environnement, et le type de changement d'environnement est difficilement prédictible à l'échelle de l'agent. Nous avons montré que l'algorithme \textsc{mEDEA} favorise effectivement la sélection de stratégies capables de survivre dans l'environnement courant indépendemment des changements pouvant avoir lieu.
	\item Finalement, nous avons étudié la possibilité d'évoluer des comportements altruistes. Cette capacité est nécessaire pour faire face à certains environnements où les comportements égoïstes épuisent des ressources nécessaires, et mènent à l'extinction de la population. Cependant l'évolution de tels comportements peut paraître au premier abord contraire aux intérêts égoïstes de chaque agent. Cette question a aussi été étudiée en biologie, où l'importance de la fitness inclusive (évaluation de la qualité d'un agent prenant en compte le succès de ses parents) a été montrée. Les expériences conduites dans ce cadre nous ont montrées l'évolution de comportement altruiste dans des conditions similaires aux prédictions faîtes en biologie théorique. De plus nous avons montré que l'altruisme a un impact sur l'émergence de comportements avec différents degrés de dispersions.
\end{itemize}

Les travaux présentés dans cette thèse montrent effectivement l'ensemble minimal de mécanismes nécessaires à l'auto-adaptation d'un groupe de robots. Ces mécanismes sont implémentés dans un algorithme nommé \textsc{mEDEA}. Nous avons montré que cet algorithme ne requière aucune modification pour faire face à de multiples environnements. De futurs travaux sont envisagés pour étudier l'ajout de nouveaux mécanismes à l'algorithme \textsc{mEDEA}. De plus de nouveaux types d'environnements peuvent être envisagés pour tester les facultés d'adaptation (les environnements où la capacité à se partager la réalisation d'une tâche est nécessaire).




\end{doublespace}

%	The third chapter presents the issues faced by the decentralized autonomous optimization of behaviors for robots deployed in unknown environments. Different methods known to partially address these are reviewed. After highlighting the strength and weakness of each method, the EDEA domain is introduced. 
%	
%	In the fourth chapter the minimal EDEA algorithm, termed \textsc{mEDEA} is presented. The ability of \textsc{mEDEA} to reach consensus is presented in simulation and real world experiments.
%	
%	The fifth chapter is focused on the capacity of \textsc{mEDEA} to address changing environments. Notably, the main evolutionary dynamics of the algorithm are shown.
%	
%	The sixth chapter shows the evolution of altruistic behaviors by the \textsc{mEDEA} algorithm in front of challenging environments. Moreover, the mechanism at play during the evolution of altruistic behaviors are analyzed.
%	
%	The seventh chapter conclude this Ph.D. thesis. A discussion on the work done is presented, and perspectives for future works are highlighted. 

\end{document}